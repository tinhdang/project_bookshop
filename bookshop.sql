-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2016 at 10:06 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `acounts`
--

CREATE TABLE `acounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `acounts`
--

INSERT INTO `acounts` (`id`, `username`, `email`, `password`, `created_at`, `updated_at`) VALUES
(11, 'tinhdang4', 'tinhdang4@gmail.com', '123456', '2016-12-20 20:41:58', '2016-12-21 20:03:12'),
(13, 'tinhdang', 'tinhdang22@gmail.com', '123456', '2016-12-21 02:30:56', '2016-12-21 05:59:37'),
(14, 'tinhdang2', 'admin@gmail.com', '123456', '2016-12-21 10:01:21', '2016-12-21 10:01:21'),
(15, 'tinhdang3', 'tinhdang@gmail.com', '123456', '2016-12-21 20:00:12', '2016-12-21 20:00:12'),
(16, 'tinhdang123', 'tinhdang@gmail.com', '123456', '2016-12-21 20:02:35', '2016-12-21 20:02:35');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `keywords`, `created_at`, `updated_at`) VALUES
(1, 'Sách Tiếng Anh', 'sach-tieng-anh', '2016-11-16 19:11:43', '2016-11-27 23:21:48'),
(2, 'Tuyển tập', 'tuyen-tap', '2016-11-17 18:19:30', '2016-11-27 23:21:52'),
(3, 'Sách trong nước', 'sach-trong-nuoc', '2016-11-17 19:25:33', '2016-11-27 23:21:45'),
(4, 'Theo Tác giả', 'theo-tac-gia', '2016-11-17 23:42:19', '2016-11-27 23:21:42'),
(5, 'Sách Tiếng Việt', 'sach-tieng-viet', '2016-11-18 02:07:27', '2016-11-27 23:21:34');

-- --------------------------------------------------------

--
-- Table structure for table `jokes`
--

CREATE TABLE `jokes` (
  `id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `jokes`
--

INSERT INTO `jokes` (`id`, `body`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 'text2', 1, '0000-00-00 00:00:00', '2016-12-19 21:41:15');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_11_16_025737_create_categories_table', 1),
('2016_11_16_030624_create_sub__cates_table', 2),
('2016_11_16_031328_create_sub__cates_table', 3),
('2016_11_16_031358_create_products_table', 4),
('2016_11_23_065419_create_slides_table', 5),
('2016_11_29_065038_create_my_models_table', 6),
('2016_12_19_020805_create_slides_table', 7),
('2016_12_19_021052_create_orders_table', 8),
('2016_12_20_012533_create_jokes_table', 9),
('2016_12_20_060906_create_users_table', 10),
('2016_12_21_024744_create_acounts_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_sub` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `sale` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_sub` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `sale`, `number`, `picture`, `content`, `keywords`, `id_sub`, `status`, `created_at`, `updated_at`) VALUES
(7, 'John Green - The Collection', 800000, 20, 1, '1479799149-thumb1.gif', '<p>ohn Green - The Collection includes five novels from the critically acclaimed, best-selling master of modern storytelling, brought together for the first time. Includes The Fault in Our Stars, Looking for Alaska, Paper Towns, An Abundance of Katherines and Will Grayson, Will Grayson (co-written with David Levithan). &#39;You laugh, you cry, and then you come back for more&#39; - Markus Zusak, author of The Book Thief &#39;Funny, rude and original&#39; - New York Times &#39;Fun, challengingly complex and entirely entertaining&#39; - Kirkus Reviews</p>\r\n', 'john-green-the-collection', 13, 0, '2016-11-17 02:44:10', '2016-11-23 19:24:16'),
(9, 'The Fault In Our Stars', 60000, 10, 10, '1479808705-1_p12.jpg', '<p>The Fault In Our StarsThe Fault In Our StarsThe Fault In Our StarsThe Fault In Our StarsThe Fault In Our StarsThe Fault In Our StarsThe Fault In Our Stars</p>\r\n', 'the-fault-in-our-stars', 14, 0, '2016-11-17 02:56:56', '2016-11-23 19:24:01'),
(12, 'The Big Short: Film Tie-In', 200000, 10, 2, '1479799617-1_p14.jpg', '<p style="text-align:justify"><span style="color:rgb(255, 102, 0); font-size:medium"><strong>The Big Short: Film Tie-In</strong></span></p>\r\n\r\n<p style="text-align:justify">From the jungles of the trading floor to the casinos of Las Vegas,&nbsp;<em>The Big Short,&nbsp;</em>Michael Lewis&#39;s No.1 bestseller, tells the outrageous story of the misfits, renegades and visionaries who saw that the biggest credit bubble of all time was about to burst, bet against the banking system - and made a killing.</p>\r\n', 'the-big-short-film-tie-in', 2, 1, '2016-11-22 00:26:46', '2016-11-23 19:23:48'),
(13, 'Lẽ Phải Của Phi Lý Trí (Tái Bản)', 87000, 10, 2, '1479806531-1_p10.jpg', '<div class="col-xs-8" style="box-sizing: border-box; float: left; position: relative; min-height: 1px; padding-left: 15px; padding-right: 15px; width: 780px; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">\r\n<div class="product-content-detail" style="box-sizing: border-box; text-align: justify;">\r\n<div class="content js-content expand" id="gioi-thieu" style="box-sizing: border-box; overflow: hidden; margin-bottom: 15px; max-height: none; line-height: 21px;">\r\n<p><span style="font-size:medium"><strong><span style="font-family:arial,helvetica,sans-serif">Lẽ Phải Của Phi L&yacute; Tr&iacute; (T&aacute;i Bản)</span></strong></span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif; font-size:small">Trong cuốn s&aacute;ch nền tảng của m&igrave;nh&nbsp;<strong><em>Phi L&yacute; Tr&iacute;</em></strong>, nh&agrave; khoa học x&atilde; hội Dan Ariely đ&atilde; đưa ra những ảnh hưởng đa chiều dẫn tới việc con người đưa ra những quyết định thiếu kh&ocirc;n ngoan. Giờ đ&acirc;y, trong cuốn&nbsp;<strong>Lẽ Phải Của &nbsp;Phi L&yacute; Tr&iacute;,</strong>&nbsp;&ocirc;ng h&eacute; lộ những ảnh hưởng đầy bất ngờ, cả t&iacute;ch cực lẫn ti&ecirc;u cực, m&agrave; &quot;phi l&yacute; tr&iacute;&quot;c&oacute; thể t&aacute;c động đến cuộc sống của ch&uacute;ng ta. Tập trung v&agrave;o nghi&ecirc;n cứu những h&agrave;nh vi của ch&uacute;ng ta tại nơi l&agrave;m việc v&agrave; c&aacute;c mối quan hệ của ch&uacute;ng ta, &ocirc;ng đưa ra những hiểu biết mới v&agrave; những sự thật đ&aacute;ng ngạc nhi&ecirc;n về những điều đang th&ocirc;i th&uacute;c ch&uacute;ng ta l&agrave;m việc, c&aacute;ch thức m&agrave; một h&agrave;nh động thiếu kh&ocirc;n ngoan c&oacute; thể trở th&agrave;nh một th&oacute;i quen, c&aacute;ch m&agrave; ch&uacute;ng ta học để y&ecirc;u thương những người m&agrave; ch&uacute;ng ta sống c&ugrave;ng, v&agrave; c&ograve;n nhiều hơn thế nữa.</span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif; font-size:small">Sử dụng những phương ph&aacute;p th&iacute; nghiệm giống như những g&igrave; đ&atilde; khiến cuốn s&aacute;ch&nbsp;<strong><em>Phi L&yacute; Tr&iacute;</em></strong>&nbsp;trở th&agrave;nh một trong những cuốn s&aacute;ch được b&agrave;n luận tới nhiều nhất trong v&agrave;i năm trở lại đ&acirc;y, Ariely sử dụng dữ liệu từ những th&iacute; nghiệm th&uacute; vị v&agrave; độc đ&aacute;o để đưa ra những kết luận hấp dẫn về c&aacute;ch thức - v&agrave; nguy&ecirc;n nh&acirc;n tại sao - ch&uacute;ng ta h&agrave;nh động như vậy. Từ những th&aacute;i độ tại nơi l&agrave;m việc của ch&uacute;ng ta, cho tới những mối quan hệ l&atilde;ng mạn, tới việc ch&uacute;ng ta lu&ocirc;n t&igrave;m kiếm mục đ&iacute;ch cuộc đời m&igrave;nh, Ariely l&yacute; giải c&aacute;ch thức ph&aacute; vỡ những khu&ocirc;n mẫu bi quan trong suy nghĩ v&agrave; h&agrave;nh vi của ch&uacute;ng ta để đưa ra những quyết định tốt hơn.</span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif; font-size:small"><strong><em>Lẽ Phải Của Phi L&yacute; Tr&iacute;</em></strong>&nbsp;sẽ thay đổi c&aacute;ch thức ch&uacute;ng ta nhận thức bản th&acirc;n trong c&ocirc;ng việc v&agrave; trong gia đ&igrave;nh - v&agrave; soi x&eacute;t c&aacute;c h&agrave;nh vi phi l&yacute; tr&iacute; của ch&uacute;ng ta dưới một thứ sắc th&aacute;i &aacute;nh s&aacute;ng ho&agrave;n to&agrave;n mới mẻ.</span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif; font-size:small">Đọc cuốn s&aacute;ch bạn sẽ biết:</span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif; font-size:small">- Tại sao những khoản tiền thưởng lớn lại c&oacute; thể khiến c&aacute;c CEO l&agrave;m việc k&eacute;m hiệu quả hơn?</span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif; font-size:small">- L&agrave;m c&aacute;ch n&agrave;o m&agrave; những định hướng rối rắm lại hữu &iacute;ch đối với ch&uacute;ng ta?</span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif; font-size:small">- Tại sao việc trả th&ugrave; lại quan trọng?</span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif; font-size:small">- Tại sao c&oacute; một sự kh&aacute;c biệt rất lớn giữa điều m&agrave; ch&uacute;ng ta nghĩ l&agrave; sẽ khiến ch&uacute;ng ta hạnh ph&uacute;c v&agrave; những thứ thực sự khiến ch&uacute;ng ta hạnh ph&uacute;c?</span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif; font-size:small">V&agrave; rất nhiều ph&aacute;t hiện th&uacute; vị kh&aacute;c...</span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif; font-size:small"><strong><em>&ldquo;Dan Ariely l&agrave; một thi&ecirc;n t&agrave;i trong việc am hiểu h&agrave;nh vi con người: kh&ocirc;ng một nh&agrave; kinh tế n&agrave;o c&oacute; thể giỏi hơn &ocirc;ng trong việc lột trần v&agrave; l&yacute; giải những nguy&ecirc;n nh&acirc;n ẩn s&acirc;u ph&iacute;a sau những h&agrave;nh động kỳ quặc của con người.&rdquo;</em></strong>&nbsp;-<em>&nbsp;James Surowiecki, t&aacute;c giả cuốn&nbsp;Tr&iacute; Tuệ Đ&aacute;m Đ&ocirc;ng.</em></span></p>\r\n\r\n<div style="box-sizing: border-box;"><span style="font-family:arial,helvetica,sans-serif; font-size:small">Mời c&aacute;c bạn đ&oacute;n đọc!</span></div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<p>&nbsp;</p>\r\n', 'le-phai-cua-phi-ly-tri', 14, 1, '2016-11-22 02:22:11', '2016-11-23 19:23:39'),
(14, 'Tuần Làm Việc 4 Giờ (Tái Bản 2015)', 200000, 20, 23, '1479806727-img893_3_4.jpg', '<p style="text-align:justify"><span style="color:rgb(255, 102, 0); font-size:medium"><strong>Tuần L&agrave;m Việc 4 Giờ</strong></span></p>\r\n\r\n<p style="text-align:justify"><em>Nếu bạn biết c&aacute;ch quản l&iacute; thời gian, c&aacute;c mục ti&ecirc;u cũng như thứ tự ưu ti&ecirc;n c&aacute;c c&ocirc;ng việc một c&aacute;ch hợp l&iacute; th&igrave; bạn ho&agrave;n to&agrave;n c&oacute; khả năng tạo ra nguồn thu nhập đầy đủ d&ugrave; chỉ l&agrave;m việc 4 giờ mỗi tuần. V&agrave; thế l&agrave;, tạm biệt nh&eacute; - những ng&agrave;y l&agrave;m việc 8 tiếng đồng hồ nặng nhọc</em>!</p>\r\n\r\n<p style="text-align:justify">Cuốn s&aacute;ch THE 4-HOUR WORK WEEK đ&atilde; l&agrave;m dấy l&ecirc;n một l&agrave;n s&oacute;ng tư tưởng mới mẻ tại thung lũng Silicon. Marc Andreesen - chuy&ecirc;n gia về tối ưu ho&aacute; năng suất l&agrave;m việc c&aacute; nh&acirc;n đồng thời l&agrave; một trong những&nbsp;<a href="http://tiki.vn/doanh-nhan/c483" style="box-sizing: border-box; background-color: transparent; color: rgb(51, 122, 183); text-decoration: none;">doanh nh&acirc;n</a>&nbsp;nổi tiếng nhất ở trung t&acirc;m c&ocirc;ng nghệ cao của thế giới trong một b&agrave;i phỏng vấn cho biết: &quot;Về cơ bản, việc Tim đ&atilde; l&agrave;m l&agrave; tập hợp v&agrave; chắt lọc những học thuyết về quản l&yacute; thời gian c&ugrave;ng năng suất lao động trong v&ograve;ng 2, 3 thập kỷ vừa qua, sau đ&oacute; tối ưu ho&aacute; n&oacute; v&agrave; đưa ra những kh&aacute;i niệm mới, rất hữu dụng&quot;.</p>\r\n\r\n<p style="text-align:justify"><em>B&iacute; quyết m&agrave; t&aacute;c giả ph&aacute;t hiện ra được g&oacute;i gọn trong chữ DEAL tương ứng với bốn bước:</em></p>\r\n\r\n<p style="text-align:justify"><strong>D: Definition&nbsp;</strong>(X&aacute;c định) Trong bước n&agrave;y Tim Ferriss gi&uacute;p bạn thay đổi ho&agrave;n to&agrave;n những suy nghĩ sai lầm v&agrave; đưa ra những qui tắc v&agrave; mục đ&iacute;ch của một phương ph&aacute;p mới. Bạn sẽ c&oacute; c&acirc;u trả lời cho c&acirc;u hỏi những nh&agrave; gi&agrave;u mới (new rich) l&agrave; ai v&agrave; họ h&agrave;nh động như thế n&agrave;o.</p>\r\n\r\n<p style="text-align:justify"><strong>E: Elimination&nbsp;</strong>(Loại bỏ) Giết chết những quan điểm lỗi thời về c&aacute;ch quản l&iacute; thời gian. Bằng c&aacute;ch sử dụng những kĩ thuật kh&aacute;c thường của những nh&agrave; gi&agrave;u mới: &aacute;p dụng &ldquo;sự thờ ơ c&oacute; chọn lựa&rdquo; &ndash; loại bỏ những thứ rườm r&agrave; kh&ocirc;ng cần thiết, chỉ chuy&ecirc;n t&acirc;m v&agrave;o những việc thực sự quan trọng, bạn c&oacute; thể tăng năng suất lao động l&ecirc;n &iacute;t nhất mười lần. Phần n&agrave;y cũng giải th&iacute;ch nh&acirc;n tố đầu ti&ecirc;n tạo n&ecirc;n một phong c&aacute;ch sống sung t&uacute;c, đ&oacute; l&agrave; thời gian.</p>\r\n\r\n<p style="text-align:justify"><strong>A: Automation&nbsp;</strong>(Tự động h&oacute;a) C&oacute; những phương ph&aacute;p n&agrave;o khiến tiền của cứ thế tự động chảy v&agrave;o t&uacute;i bạn? Trong bước n&agrave;y bạn sẽ tiếp tục t&igrave;m hiểu về thu nhập (income): nh&acirc;n tố thứ hai tạo n&ecirc;n một phong c&aacute;ch sống sung t&uacute;c, thoải m&aacute;i v&agrave; đ&acirc;u l&agrave; điểm kh&aacute;c nhau ch&iacute;nh giữa thu nhập tương đối v&agrave; thu nhập tuyệt đối?</p>\r\n\r\n<p style="text-align:justify"><strong>L: Liberation&nbsp;</strong>(Tự do) đ&acirc;y l&agrave; bản tuy&ecirc;n ng&ocirc;n về t&iacute;nh lưu động. Bạn sẽ học được c&aacute;ch điều khiển c&ocirc;ng việc từ xa trong khi đang ở trong k&igrave; nghỉ. &ldquo;Tự do&rdquo; ở đ&acirc;y kh&ocirc;ng c&oacute; nghĩa l&agrave; c&aacute;c k&igrave; nghỉ rẻ tiền, n&oacute; &aacute;m chỉ c&aacute;ch bạn tho&aacute;t khỏi c&aacute;c mối quan hệ r&agrave;ng buộc bạn với một địa điểm cố định. Phần n&agrave;y cũng đề cập đến nh&acirc;n tố cuối c&ugrave;ng tạo n&ecirc;n một cuộc sống thoải m&aacute;i: t&iacute;nh lưu động (mobility).</p>\r\n\r\n<p style="text-align:justify">Ngo&agrave;i rất nhiều &yacute; kiến khen ngợi, đồng t&igrave;nh với những b&iacute; quyết n&agrave;y, cũng c&oacute; một số &yacute; kiến phản đối kh&aacute; gay gắt. C&oacute; người cho rằng cuốn s&aacute;ch đang cố rao giảng một thứ phương ch&acirc;m sống kh&ocirc;ng c&ograve;n mới mẻ: &ldquo;Tận hưởng cuộc sống ngay khi bạn c&ograve;n c&oacute; thể.&rdquo; (Đ&atilde; được Thoreau đưa ra c&aacute;ch đấy 2 thế kỷ!) V&agrave; đưa ra giải ph&aacute;p để c&oacute; thể thực hiện lối sống &ldquo;nh&agrave;n hạ&rdquo; đ&oacute; l&agrave; bằng c&aacute;ch thu&ecirc; l&agrave;m b&ecirc;n ngo&agrave;i (outsource) tất cả những c&ocirc;ng việc c&oacute; thể, do vậy bạn giảm thiểu những việc bạn phải l&agrave;m v&agrave; nhờ đ&oacute; c&oacute; thời gian để hưởng thụ cuộc sống.</p>\r\n\r\n<p style="text-align:justify">Những độc giả n&agrave;y lập luận rằng: như vậy, thực ra chẳng c&oacute; c&ocirc;ng việc n&agrave;o mất đi cả, n&oacute; chỉ được thực hiện bởi những người kh&aacute;c m&agrave; th&ocirc;i. V&agrave; nếu ai cũng l&agrave;m theo lời khuy&ecirc;n của Tim Ferriss th&igrave; cuối c&ugrave;ng ai sẽ l&agrave; người l&agrave;m những việc đ&oacute;?</p>\r\n\r\n<p style="text-align:justify">Th&ecirc;m nữa, b&iacute; quyết l&agrave;m gi&agrave;u m&agrave; Tim Ferriss cung cấp trong quyển s&aacute;ch n&agrave;y tập trung v&agrave;o việc th&agrave;nh lập một doanh nghiệp điện tử, l&agrave;m gi&agrave;u nhờ kinh doanh qua mạng (c&ocirc;ng thức phổ biến gi&uacute;p l&agrave;m n&ecirc;n c&aacute;c nh&agrave; gi&agrave;u mới). C&ocirc;ng thức n&agrave;y kh&oacute; c&oacute; thể &aacute;p dụng cho mọi người, v&igrave; kh&ocirc;ng phải ai cũng được th&ocirc;ng minh v&agrave; c&oacute; điều kiện tốt như Ferriss.</p>\r\n\r\n<p style="text-align:justify">Những luồng dư luận c&oacute; vẻ &ldquo;đối nhau chan ch&aacute;t&rdquo; với cuốn s&aacute;ch cũng l&agrave; điều dễ hiểu, bởi kh&ocirc;ng c&oacute; g&igrave; l&agrave; ho&agrave;n to&agrave;n tuyệt đối, cũng như Tim Ferriss kh&ocirc;ng tham vọng khiến cả thế giới được &ldquo;xả hơi thoải m&aacute;i&rdquo; chỉ nhờ cuốn s&aacute;ch của anh.</p>\r\n\r\n<p style="text-align:justify">Điều đ&aacute;ng ghi nhận nhất ở đ&acirc;y c&oacute; lẽ l&agrave; đa số c&aacute;c độc giả đều đ&aacute;nh gi&aacute; cao cuốn s&aacute;ch của Tim Ferriss với nhận x&eacute;t: cuốn s&aacute;ch c&oacute; lối viết kiểu tự sự dễ đọc, bố cục mạch lạc dễ hiểu v&agrave; đ&atilde; cung cấp rất nhiều thủ thuật hay gi&uacute;p những người l&agrave;m c&ocirc;ng việc c&ocirc;ng sở tiết kiệm thời gian v&agrave; giảm stress (v&iacute; dụ thủ thuật kiểm tra v&agrave; trả lời email 2 lần một ng&agrave;y v&agrave;o những giờ x&aacute;c định). Họ cũng cho rằng cuốn s&aacute;ch gi&uacute;p họ thiết lập một cuộc sống &ldquo;ngăn nắp hơn&rdquo; v&agrave; vui vẻ hơn nhiều.</p>\r\n\r\n<p style="text-align:justify">V&agrave; điều đ&oacute; th&ecirc;m một lần nữa khẳng định th&ecirc;m: mọi b&iacute; quyết chỉ l&agrave; b&iacute; quyết, c&ograve;n &aacute;p dụng như thế n&agrave;o, t&ugrave;y thuộc ho&agrave;n to&agrave;n v&agrave;o bạn, m&agrave; khởi đầu cho tất cả, bạn cứ tự nhủ với m&igrave;nh rằng: c&oacute; thể l&agrave;m n&oacute; tốt đẹp v&agrave; ổn thỏa hơn hiện tại rất nhiều lần!</p>\r\n', 'tuan-lam-viec-4-gio', 10, 1, '2016-11-22 02:25:27', '2016-11-23 19:23:20'),
(15, 'Tony Buổi Sáng - Trên Đường Băng', 75000, 30, 10, '1479808637-1_p17.jpg', '<p style="text-align:justify"><span style="color:rgb(255, 102, 0); font-size:medium"><strong>Tr&ecirc;n Đường Băng</strong></span></p>\r\n\r\n<p style="text-align:justify"><em>&quot;Khi c&ograve;n trẻ, h&atilde;y ra ngo&agrave;i nhiều hơn ở nh&agrave;. H&atilde;y nh&agrave;o v&ocirc; xin người kh&aacute;c &ldquo;b&oacute;c hết, lột sạch&rdquo; khả năng của m&igrave;nh. Chỉ sợ bất t&agrave;i nộp hồ sơ &ldquo;xin việc&rdquo;, m&agrave; chả ai th&egrave;m cho, chả ai th&egrave;m b&oacute;c lột. Khi đ&atilde; được b&oacute;c v&agrave; lột hết, d&ugrave; sau n&agrave;y đi đ&acirc;u, l&agrave;m g&igrave;, bạn đều cực kỳ th&agrave;nh c&ocirc;ng. V&igrave; năng lực được trui r&egrave;n trong qu&aacute; tr&igrave;nh l&agrave;m cho người kh&aacute;c. Sự chăm chỉ, t&iacute;nh kỷ luật, quen tay quen ch&acirc;n, quen ng&aacute;p, quen lười&hellip; cũng từ c&ocirc;ng việc m&agrave; ra. Mọi &ocirc;ng chủ vĩ đại đều từng l&agrave; những người l&agrave;m c&ocirc;ng ở vị tr&iacute; thấp nhất. Họ đều rẽ tr&aacute;i trong khi mọi người rẽ phải. Họ c&oacute; những quyết định kh&ocirc;ng theo đ&aacute;m đ&ocirc;ng, kh&ocirc;ng cam chịu sống một cuộc đời tầm thường, nhạt nh&ograve;a&hellip; rồi chết.</em></p>\r\n\r\n<p style="text-align:justify"><em>C&ograve;n những bạn thu nhập 6 triệu cũng t&uacute;ng thiếu, 20 triệu cũng đi vay mượn để ti&ecirc;u d&ugrave;ng, th&igrave; th&ocirc;i, cuộc đời họ chấm dứt giấc mơ lớn. Tiền nong c&aacute; nh&acirc;n quản l&yacute; kh&ocirc;ng được, th&igrave; l&agrave;m sao m&agrave; quản trị t&agrave;i ch&iacute;nh một cơ nghiệp lớn?&rdquo;. Tư duy thế n&agrave;o th&igrave; n&oacute; ra số phận thế đ&oacute;.&quot;</em></p>\r\n\r\n<p style="text-align:justify"><strong>(Tony buổi s&aacute;ng)</strong></p>\r\n\r\n<p style="text-align:justify"><strong>Tr&ecirc;n đường băng</strong>&nbsp;l&agrave; tập hợp những b&agrave;i viết được ưa th&iacute;ch tr&ecirc;n Facebook của&nbsp;<strong>Tony Buổi S&aacute;ng</strong>. Nhưng kh&aacute;c với một tập tản văn th&ocirc;ng thường, nội dung c&aacute;c b&agrave;i được chọn lọc c&oacute; chủ đ&iacute;ch, nhằm chuẩn bị về tinh thần, kiến thức&hellip;cho c&aacute;c bạn trẻ v&agrave;o đời. S&aacute;ch gồm 3 phần: &ldquo;Chuẩn bị h&agrave;nh trang&rdquo;, &ldquo;Trong ph&ograve;ng chờ s&acirc;n bay&rdquo; v&agrave; &ldquo;L&ecirc;n m&aacute;y bay&rdquo;, tương ứng với những qu&aacute; tr&igrave;nh một bạn trẻ phải trải qua trước khi &ldquo;cất c&aacute;nh&rdquo; tr&ecirc;n đường băng cuộc đời, bay v&agrave;o bầu trời cao rộng.</p>\r\n\r\n<p style="text-align:justify">Những b&agrave;i viết của Tony sinh động, thiết thực, h&agrave;i hước v&agrave; xuất ph&aacute;t từ c&aacute;i t&acirc;m trong s&aacute;ng của một người đi trước nhiều kinh nghiệm. Anh viết về th&aacute;i độ với sự học v&agrave; kiến thức n&oacute;i chung, c&aacute;ch ứng ph&oacute; với những trắc trở thử th&aacute;ch khi đi l&agrave;m, c&aacute;ch sống h&agrave;o sảng nghĩa t&igrave;nh văn minh&hellip;truyền cảm hứng cho c&aacute;c bạn trẻ sống hết m&igrave;nh, trọn vẹn từng ph&uacute;t gi&acirc;y. Tuy đối tượng độc giả ch&iacute;nh m&agrave; cuốn s&aacute;ch hướng đến l&agrave; c&aacute;c bạn trẻ, nhưng độc giả lớn tuổi hơn vẫn c&oacute; thể đọc s&aacute;ch để hiểu v&agrave; c&oacute; c&aacute;ch hỗ trợ con em m&igrave;nh một c&aacute;ch đ&uacute;ng đắn, chứ kh&ocirc;ng &ldquo;ủ&rdquo; con qu&aacute; kỹ để rồi tạo ra một thế hệ yếu ớt, kh&ocirc;ng biết tự lập. Những người đi l&agrave;m nhiều năm đọc s&aacute;ch cũng c&oacute; thể nh&igrave;n lại con đường đi của m&igrave;nh, tự ngẫm đ&oacute; đ&atilde; phải l&agrave; con đường m&igrave;nh muốn đi chưa, bởi thay đổi kh&ocirc;ng bao giờ l&agrave; qu&aacute; muộn.</p>\r\n', 'tony-buoi-sang-tren-duong-bang', 9, 1, '2016-11-22 02:57:17', '2016-11-23 19:23:06'),
(16, 'Sống Thời Bao Cấp', 87000, 10, 2, '1479870996-8932000124177.jpg', '<div class="m_96555302933771056yiv9240176066MsoListParagraph" id="m_96555302933771056yui_3_16_0_ym19_1_1479357892426_80381" style="box-sizing: border-box; margin: 0px; padding: 0px; font-family: OpenSansRegular, Arial, Helvetica, sans-serif; font-size: 12px; background-color: rgb(249, 249, 249); text-align: justify;">Sống Thời Bao Cấp</div>\r\n\r\n<div class="m_96555302933771056yiv9240176066MsoListParagraph" style="box-sizing: border-box; margin: 0px; padding: 0px; font-family: OpenSansRegular, Arial, Helvetica, sans-serif; font-size: 12px; background-color: rgb(249, 249, 249); text-align: justify;">&nbsp;</div>\r\n\r\n<div class="m_96555302933771056yiv9240176066MsoListParagraph" style="box-sizing: border-box; margin: 0px; padding: 0px; font-family: OpenSansRegular, Arial, Helvetica, sans-serif; font-size: 12px; background-color: rgb(249, 249, 249); text-align: justify;">\r\n<div class="m_96555302933771056yiv9240176066MsoListParagraph" style="box-sizing: border-box; margin: 0px; padding: 0px;"><strong>Sống thời bao cấp</strong>: Thời bao cấp trong mỗi người lớn tuổi l&agrave; một cảm x&uacute;c kh&aacute;c nhau, C&oacute; người nhớ về như một giai đoạn lạc hậu v&agrave; bảo thủ, người cho đ&oacute; l&agrave; thời của những ấu trĩ hồn nhi&ecirc;n, đ&aacute;ng tr&aacute;ch nhưng kh&ocirc;ng đ&aacute;ng giận. Nh&agrave; văn Ng&ocirc; Minh đ&atilde; l&agrave;m &ldquo;sống lại&rdquo; thời bao cấp&nbsp; - một giai đoạn ph&aacute;t triển của đất nước sau chiến tranh &ndash; với c&aacute;i nh&igrave;n ri&ecirc;ng, vừa buồn cười vừa cay đắng, vừa giận vừa thương, vừa muốn qu&ecirc;n đi vừa kh&ocirc;ng thể kh&ocirc;ng nhớ.</div>\r\n\r\n<div class="m_96555302933771056yiv9240176066MsoListParagraph" style="box-sizing: border-box; margin: 0px; padding: 0px;">&nbsp;</div>\r\n\r\n<div class="m_96555302933771056yiv9240176066MsoListParagraph" style="box-sizing: border-box; margin: 0px; padding: 0px;">Gi&aacute;o sư&nbsp;<a href="https://vi.wikipedia.org/wiki/Tr%E1%BA%A7n_V%C4%83n_Th%E1%BB%8D" rel="nofollow" style="box-sizing: border-box; margin: 0px; padding: 0px; background: transparent; color: rgb(100, 100, 100); text-decoration: none; transition: all 300ms ease-in 0s;" target="_blank" title="Trần Văn Thọ">Trần Văn Thọ</a>&nbsp;viết về t&igrave;nh trạng kinh tế 10 năm đầu sau chiến tranh:&nbsp;<em>&quot;Mười năm sau 1975 l&agrave; một trong những giai đoạn tối tăm nhất trong lịch sử Việt Nam. Chỉ n&oacute;i về mặt kinh tế, l&agrave; một nước n&ocirc;ng nghiệp (năm 1980, 80% d&acirc;n số sống ở n&ocirc;ng th&ocirc;n v&agrave; 70% lao động l&agrave; n&ocirc;ng d&acirc;n) nhưng Việt Nam thiếu ăn, nhiều người phải ăn bo bo trong thời gian d&agrave;i. Lượng lương thực t&iacute;nh tr&ecirc;n đầu người giảm li&ecirc;n tục từ năm 1976 đến 1979, sau đ&oacute; tăng trở lại nhưng cho đến năm 1981 vẫn kh&ocirc;ng hồi phục lại mức năm 1976. C&ocirc;ng thương nghiệp cũng đ&igrave;nh trệ, sản xuất đ&igrave;nh đốn, vật dụng hằng ng&agrave;y thiếu thốn, cuộc s&ocirc;ng của người d&acirc;n v&ocirc; c&ugrave;ng khốn kh&oacute;. Ngo&agrave;i những kh&oacute; khăn của một đất nước sau chiến tranh v&agrave; t&igrave;nh h&igrave;nh quốc tế bất lợi, nguy&ecirc;n nh&acirc;n ch&iacute;nh của t&igrave;nh trạng n&oacute;i tr&ecirc;n l&agrave; do sai lầm trong ch&iacute;nh s&aacute;ch, chiến lược ph&aacute;t triển, trong đ&oacute; nổi bật nhất l&agrave; sự n&oacute;ng vội trong việc &aacute;p dụng m&ocirc; h&igrave;nh x&atilde; hội chủ nghĩa trong kinh tế ở miền Nam... Nguy cơ thiếu ăn k&eacute;o d&agrave;i v&agrave; những kh&oacute; khăn c&ugrave;ng cực kh&aacute;c l&agrave;m ph&aacute;t sinh hiện tượng &quot;ph&aacute; r&agrave;o&quot; trong n&ocirc;ng nghiệp, trong mậu dịch v&agrave; trong việc quyết định gi&aacute; cả lương thực đ&atilde; cải thiện t&igrave;nh h&igrave;nh tại một số địa phương&rdquo;</em></div>\r\n</div>\r\n', 'song-thoi-bao-cap', 11, 1, '2016-11-22 20:16:36', '2016-11-23 19:22:59'),
(17, 'Ngày Xưa Có Một Chuyện Tình', 115000, 25, 23, '1479873488-1_p22.jpg', '<p style="text-align:justify"><span style="color:rgb(255, 102, 0); font-size:medium"><strong>Ng&agrave;y Xưa C&oacute; Một Chuyện T&igrave;nh</strong></span></p>\r\n\r\n<p style="text-align:justify">Đ&uacute;ng như t&ecirc;n gọi, đ&acirc;y l&agrave; cuốn s&aacute;ch về&nbsp;<a href="http://tiki.vn/tieu-thuyet-tinh-cam-lang-man/c844" style="box-sizing: border-box; background-color: transparent; color: rgb(51, 122, 183); text-decoration: none;">t&igrave;nh y&ecirc;u</a>, được viết theo một phong c&aacute;ch ho&agrave;n to&agrave;n kh&aacute;c lạ với nh&agrave; văn&nbsp;<strong><a href="http://tiki.vn/author/nguyen-nhat-anh.html" style="box-sizing: border-box; background-color: transparent; color: rgb(51, 122, 183); text-decoration: none;">Nguyễn Nhật &Aacute;nh</a></strong>&nbsp;từ trước đến nay.</p>\r\n\r\n<p style="text-align:justify">Ng&agrave;y xưa c&oacute; một chuyện t&igrave;nh l&agrave; một c&acirc;u chuyện cảm động khi người ta y&ecirc;u nhau, nỗi kh&aacute;t khao một hạnh ph&uacute;c &ecirc;m đềm ấm &aacute;p đến thế; hay đơn giản chỉ l&agrave; chuyện ba người - anh, em, v&agrave; người ấy&hellip;?</p>\r\n\r\n<p style="text-align:justify">Khi mở s&aacute;ch ra, độc giả sẽ được chứng kiến l&agrave;n gi&oacute; t&igrave;nh y&ecirc;u chảy qua như rải nắng tr&ecirc;n khu&ocirc;n mặt m&ugrave;a đ&ocirc;ng của c&ocirc; g&aacute;i; nụ h&ocirc;n đầu ti&ecirc;n ngọt mật, c&aacute;i &ocirc;m đầu ti&ecirc;n, những giọt nước mắt v&agrave; c&aacute;i &ocirc;m xiết cuối c&ugrave;ng của tấm t&igrave;nh người y&ecirc;u người&hellip; V&agrave; người đọc sẽ t&igrave;m thấy c&acirc;u trả lời, cho ri&ecirc;ng m&igrave;nh.</p>\r\n\r\n<p style="text-align:justify"><em>&quot;- Miền n&egrave;.</em></p>\r\n\r\n<p style="text-align:justify"><em>- G&igrave; hở Ph&uacute;c?</em></p>\r\n\r\n<p style="text-align:justify"><em>- C&oacute; chuyện n&agrave;y n&egrave;.</em></p>\r\n\r\n<p style="text-align:justify"><em>- Chuyện g&igrave; vậy?</em></p>\r\n\r\n<p style="text-align:justify"><em>- Ở trong lớp m&igrave;nh ấy m&agrave;.</em></p>\r\n\r\n<p style="text-align:justify"><em>- Trong lớp m&igrave;nh sao?</em></p>\r\n\r\n<p style="text-align:justify"><em>- Lần n&agrave;y t&ocirc;i thấy đ&ocirc;i l&ocirc;ng m&agrave;y Miền nhướn l&ecirc;n. N&oacute; vừa hỏi vừa xo&aacute;y mắt v&agrave;o mặt t&ocirc;i, chắc n&oacute; lấy l&agrave;m lạ trước lối n&oacute;i chuyện l&ograve;ng v&ograve;ng của t&ocirc;i.</em></p>\r\n\r\n<p style="text-align:justify"><em>Ngay cả t&ocirc;i, t&ocirc;i cũng thấy t&ocirc;i kh&ocirc;ng giống mọi h&ocirc;m ch&uacute;t n&agrave;o v&agrave; ph&aacute;t hiện đ&oacute; khiến t&ocirc;i gần như nổi đi&ecirc;n l&ecirc;n với ch&iacute;nh m&igrave;nh.</em></p>\r\n\r\n<p style="text-align:justify"><em>T&ocirc;i n&oacute;i nhanh:</em></p>\r\n\r\n<p style="text-align:justify"><em>- C&oacute; một bạn trong lớp đang th&iacute;ch Miền đ&oacute;.&quot;</em></p>\r\n\r\n<p style="text-align:justify"><em>&quot;T&ocirc;i rơi v&agrave;o t&igrave;nh y&ecirc;u như thi&ecirc;n thạch bị rơi v&agrave;o lỗ đen. T&ocirc;i bị t&igrave;nh y&ecirc;u đ&oacute; nuốt chửng với một sức mạnh kh&ocirc;ng sao cưỡng lại được.</em></p>\r\n\r\n<p style="text-align:justify"><em>Suốt một thời gian d&agrave;i, t&ocirc;i trượt tr&ecirc;n t&igrave;nh y&ecirc;u như trượt tr&ecirc;n vỏ chuối, ng&acirc;y ngất, m&ecirc; man, chỉ khi n&agrave;o t&eacute; ng&atilde; th&igrave; đ&agrave; trượt đ&oacute; mới dừng lại.</em></p>\r\n\r\n<p style="text-align:justify"><em>C&oacute; c&aacute;i g&igrave; đ&oacute; l&agrave;m t&ocirc;i lạc lối. N&oacute; khiến t&ocirc;i tin rằng đạo đức l&agrave; c&aacute;i con người vẽ ra chứ kh&ocirc;ng phải l&agrave; c&aacute;i vẽ ra con người. N&oacute; khiến t&ocirc;i sẵn s&agrave;ng nổi loạn, chẳng buồn bận t&acirc;m cuộc đời m&igrave;nh rồi sẽ tr&ocirc;i dạt về đ&acirc;u, những thứ g&igrave; sẽ đổ l&ecirc;n cuộc đời m&igrave;nh. V&agrave; t&ocirc;i, thoạt đầu l&agrave; tin một c&aacute;ch ng&acirc;y thơ, về sau th&igrave; cố tin đ&oacute; l&agrave; t&igrave;nh y&ecirc;u để biện hộ cho h&agrave;nh động của m&igrave;nh. Nhưng y&ecirc;u kiểu như t&ocirc;i y&ecirc;u Ph&uacute;c th&igrave; c&agrave;ng y&ecirc;u t&ocirc;i c&agrave;ng hiểu về t&igrave;nh y&ecirc;u &iacute;t hơn...</em></p>\r\n\r\n<p style="text-align:justify"><em>C&oacute; c&aacute;i g&igrave; đ&oacute; m&ugrave; l&ograve;a, say đắm, đi&ecirc;n rồ, ảo gi&aacute;c, đẫm m&ecirc; hương trong cuộc t&igrave;nh n&agrave;y.</em></p>\r\n\r\n<p style="text-align:justify"><strong>(Tr&iacute;ch &ldquo;Ng&agrave;y xưa c&oacute; một chuyện t&igrave;nh&rdquo;)</strong></p>\r\n', 'ngay-xua-co-mot-chuyen-tinh', 15, 1, '2016-11-22 20:58:08', '2016-11-23 19:22:51'),
(18, 'Con Chó Nhỏ Mang Giỏ Hoa Hồng ', 90000, 20, 2, '1479873557-1_p21.jpg', '<p style="text-align:justify"><strong>Con Ch&oacute; Nhỏ Mang Giỏ Hoa Hồng</strong>&nbsp;l&agrave; t&aacute;c phẩm mới nhất của nh&agrave; văn chuy&ecirc;n viết cho thanh thiếu ni&ecirc;n&nbsp;<a href="http://tiki.vn/author/nguyen-nhat-anh.html" style="box-sizing: border-box; background-color: transparent; color: rgb(51, 122, 183); text-decoration: none;">Nguyễn Nhật &Aacute;nh</a>, nối tiếp sau&nbsp;<strong>Bảy bước tới m&ugrave;a h&egrave;</strong>,&nbsp;<strong><a href="http://tiki.vn/toi-thay-hoa-vang-tren-co-xanh.html" style="box-sizing: border-box; background-color: transparent; color: rgb(51, 122, 183); text-decoration: none;">T&ocirc;i thấy hoa v&agrave;ng tr&ecirc;n cỏ xanh</a></strong>&hellip; g&acirc;y s&oacute;ng gi&oacute; thị trường s&aacute;ch năm 2015.</p>\r\n\r\n<p style="text-align:justify">5 chương s&aacute;ch với 86 c&acirc;u chuyện cực kỳ th&uacute; vị v&agrave; h&agrave;i hước về 5 con ch&oacute; 5 lo&agrave;i 5 t&iacute;nh c&aacute;ch trong 1 gia đ&igrave;nh c&oacute; 3 người đều y&ecirc;u ch&uacute;ng nhưng theo từng c&aacute;ch ri&ecirc;ng của m&igrave;nh. C&aacute;c c&acirc;u chuyện về t&igrave;nh bạn giữa ch&uacute;ng với nhau, giữa ch&uacute;ng với chị Ni, ba mẹ, kh&aacute;ch đến nh&agrave;&hellip; thực sự mang lại một thế giới trong trẻo, những đoạn đời dễ thương quyến rũ tuổi mới lớn.</p>\r\n\r\n<p style="text-align:justify">Một quyển s&aacute;ch l&ocirc;i cuốn viết cho tất cả ch&uacute;ng ta: trẻ con v&agrave; người lớn. Cuộc đời của 5 con ch&oacute; nhỏ: Haili, Bat&ocirc;, Suku, &Ecirc;m&ecirc; v&agrave; Pig &nbsp;được t&aacute;i hiện như đời sống của mỗi con người: t&igrave;nh bạn,&nbsp;<a href="http://tiki.vn/tieu-thuyet-tinh-cam-lang-man/c844" style="box-sizing: border-box; background-color: transparent; color: rgb(51, 122, 183); text-decoration: none;">t&igrave;nh y&ecirc;u</a>, đam m&ecirc;, l&ograve;ng dũng cảm, sự sợ h&atilde;i, v&agrave; những ước mơ...</p>\r\n\r\n<p style="text-align:justify"><strong>Tr&iacute;ch đoạn</strong></p>\r\n\r\n<p style="text-align:justify">Suku l&agrave; một thằng c&uacute;n n&oacute;i chung ai nh&igrave;n cũng th&iacute;ch.</p>\r\n\r\n<p style="text-align:justify">Đ&ocirc;i mắt tr&ograve;n, đen lay l&aacute;y, ng&acirc;y thơ ngơ ng&aacute;c, mỗi khi nh&igrave;n ai l&agrave; khiến người ta phải động l&ograve;ng.</p>\r\n\r\n<p style="text-align:justify">C&uacute;n l&agrave; gọi theo th&oacute;i quen hồi b&eacute;, chứ thật ra ch&uacute;ng t&ocirc;i đ&atilde; sống b&ecirc;n nhau nhiều năm rồi, t&oacute;m lại đ&atilde; qua tuổi vị th&agrave;nh ni&ecirc;n từ l&acirc;u.</p>\r\n\r\n<p style="text-align:justify">D&ugrave; vậy, so với thời ni&ecirc;n thiếu bộ dạng của thằng Suku kh&ocirc;ng thay đổi l&agrave; mấy. n&oacute; chỉ c&oacute; b&eacute;o l&ecirc;n v&igrave; ăn nhiều qu&aacute;.</p>\r\n\r\n<p style="text-align:justify">Suku c&oacute; đ&ocirc;i tai d&agrave;i. L&ocirc;ng n&oacute; m&agrave;u trằng, &oacute;ng ảnh v&agrave; xoăn từng cụm, phủ d&agrave;y từ chỏm đầu đến tận c&aacute;c ng&oacute;n ch&acirc;n - tr&ocirc;ng n&oacute; giống hệt như một con cừu. Khi n&oacute; nằm im, rất nhiều người tưởng n&oacute; l&agrave; một con ch&oacute; nhồi b&ocirc;ng. Suku xinh đẹp như thế, tất nhi&ecirc;n ai cũng muốn vuốt ve. Rất nhiều người bị vẻ ngo&agrave;i đ&aacute;ng y&ecirc;u của n&oacute; đ&aacute;nh lừa, nhưng chuyện đ&oacute; t&ocirc;i sẽ kể sau...</p>\r\n', 'con-cho-nho-mang-gio-hoa-hong', 15, 1, '2016-11-22 20:59:17', '2016-11-23 19:22:42'),
(19, 'Mắt Biếc - Tái Bản 2013', 58000, 30, 3, '1479873677-1_p20.jpg', '<p style="text-align:justify"><span style="color:rgb(255, 102, 0); font-size:medium"><strong>Mắt Biếc - T&aacute;i Bản 2013</strong></span></p>\r\n\r\n<p style="text-align:justify">Một t&aacute;c phẩm được nhiều người b&igrave;nh chọn l&agrave; hay nhất của nh&agrave; văn n&agrave;y. Một t&aacute;c phẩm đang được dịch v&agrave; giới thiệu tại Nhật Bản (theo th&ocirc;ng tin từ c&aacute;c b&aacute;o)&hellip; Bởi sự trong s&aacute;ng của một t&igrave;nh cảm, bởi c&aacute;i kết th&uacute;c rất, rất buồn khi suốt c&acirc;u chuyện vẫn l&agrave; những điều vui, buồn lẫn lộn (c&aacute;i kết th&uacute;c kh&ocirc;ng như mong đợi của mọi người). Cũng bởi, mắt biếc&hellip; năm xưa nay đ&acirc;u (theo lời một b&agrave;i h&aacute;t).</p>\r\n', 'mat-biec-tai-ban-2013', 15, 1, '2016-11-22 21:01:17', '2016-11-23 19:22:29'),
(20, 'Kính Vạn Hoa  - Tập 18', 70000, 10, 10, '1479873996-1_p23.jpg', '<p style="text-align:justify">Bộ ấn phẩm&nbsp;<strong>K&iacute;nh Vạn Hoa</strong>&nbsp;phi&ecirc;n bản mới 18 tập, minh họa của họa sĩ Đỗ Ho&agrave;ng Tường.</p>\r\n\r\n<p style="text-align:justify"><strong>Nhận định</strong></p>\r\n\r\n<p style="text-align:justify">&quot;C&aacute;i khả năng &quot;h&ocirc; biến&quot; của nh&agrave; văn thật kỳ lạ. Đ&atilde; bao lần, qua những trang s&aacute;ch của m&igrave;nh,&nbsp;<a href="http://tiki.vn/author/nguyen-nhat-anh.html" style="box-sizing: border-box; background-color: transparent; color: rgb(51, 122, 183); text-decoration: none;">Nguyễn Nhật &Aacute;nh</a>&nbsp;đ&atilde; l&agrave;m cho những ng&otilde; x&oacute;m, những khu chợ, những ngọn đồi, những v&ograve;m c&acirc;y, thậm ch&iacute; một g&oacute;c nhỏ trong ng&ocirc;i nh&agrave;&hellip; th&agrave;nh một xứ thần ti&ecirc;n. Nguyễn Nhật &Aacute;nh l&agrave; một nh&agrave; thơ, một nh&agrave; thơ từ trong bản chất, trong t&acirc;m th&aacute;i. Ch&iacute;nh tư chất thi sĩ n&agrave;y đ&atilde; l&agrave;m n&ecirc;n th&agrave;nh c&ocirc;ng cho những &aacute;ng văn xu&ocirc;i thơ mộng của anh.&quot;</p>\r\n\r\n<p style="text-align:justify"><strong>(Nh&agrave; thơ &Yacute; Nhi)</strong></p>\r\n', 'kinh-van-hoa-tap-18', 15, 1, '2016-11-22 21:06:36', '2016-11-23 19:22:16'),
(21, 'The Book Thief', 320000, 20, 2, '1479874193-1_p26.jpg', '<p><span style="font-family:arial,helvetica,sans-serif; font-size:14px">It&rsquo;s just a small story really, about among other things: a girl, some words, an accordionist, some fanatical Germans, a Jewish fist-fighter, and quite a lot of thievery. . . .</span><br />\r\n<br />\r\n<span style="font-family:arial,helvetica,sans-serif; font-size:14px">Set during World War II in Germany, Markus Zusak&rsquo;s groundbreaking new novel is the story of Liesel Meminger, a foster girl living outside of Munich. Liesel scratches out a meager existence for herself by stealing when she encounters something she can&rsquo;t resist&ndash;books. With the help of her accordion-playing foster father, she learns to read and shares her stolen books with her neighbors during bombing raids as well as with the Jewish man hidden in her basement before he is marched to Dachau.</span><br />\r\n<br />\r\n<span style="font-family:arial,helvetica,sans-serif; font-size:14px">This is an unforgettable story about the ability of books to feed the soul.</span><br />\r\n<br />\r\n<em>From the Hardcover edition.</em></p>\r\n', 'the-book-thief', 14, 1, '2016-11-22 21:09:53', '2016-11-23 19:22:01'),
(22, 'Ai rồi cũng khác', 115000, 1, 23, '1479882864-1_p14.jpg', '<p>S&aacute;ch Kinh doanh</p>\r\n', 'ai-roi-cung-khac', 14, 1, '2016-11-22 21:40:07', '2016-11-23 19:21:55'),
(23, 'Làm ít Được Nhiều', 90000, 10, 3, '1479882847-1_p27.jpg', '<p style="text-align:justify"><span style="color:rgb(255, 102, 0); font-size:medium"><strong>L&agrave;m &Iacute;t Được Nhiều</strong></span></p>\r\n\r\n<p style="text-align:justify">Trong thời đại thay đổi nhanh ch&oacute;ng v&agrave; cạnh tranh gay gắt hiện nay, ch&uacute;ng ta thường tự vướng v&agrave;o một quan điểm qu&aacute; mệt mỏi l&agrave; tin rằng th&agrave;nh c&ocirc;ng chỉ đến khi phải đ&aacute;nh đổi bằng sự c&acirc;n bằng của cuộc sống. Tuy nhi&ecirc;n, hầu hết những người th&agrave;nh c&ocirc;ng lại kh&ocirc;ng nhất thiết phải l&agrave;m việc vất vả. Trong quyển<strong>&nbsp;L&agrave;m &iacute;t được nhiều</strong>, t&aacute;c giả c&oacute; s&aacute;ch b&aacute;n chạy&nbsp;<strong>Ching-Nin-Chu</strong>&nbsp;giải th&iacute;ch c&aacute;ch l&agrave;m thế n&agrave;o để giải tỏa c&aacute;i v&ograve;ng lẩn quẩn đ&oacute; v&agrave; học c&aacute;ch để vừa b&igrave;nh an vừa hiệu quả c&ugrave;ng một l&uacute;c.</p>\r\n\r\n<p style="text-align:justify">Học c&aacute;ch l&agrave;m thế n&agrave;o để:</p>\r\n\r\n<p style="text-align:justify">&bull;&nbsp;Sử dụng Bộ ba b&iacute; mật để giải ph&oacute;ng năng lực của bản th&acirc;n;</p>\r\n\r\n<p style="text-align:justify">&bull;&nbsp;Đặt m&igrave;nh v&agrave;o t&acirc;m điểm của c&ocirc;ng việc, h&agrave;nh động một c&aacute;ch tập trung, đồng thời c&acirc;n bằng cuộc sống;</p>\r\n\r\n<p style="text-align:justify">&bull;&nbsp;Đạt được mục ti&ecirc;u m&agrave; chỉ cần l&agrave;m việc v&agrave; lo lắng &iacute;t th&ocirc;i;</p>\r\n\r\n<p style="text-align:justify">&bull;&nbsp;C&oacute; thời gian v&agrave; sự thảnh thơi để hưởng thụ th&agrave;nh quả lao động.</p>\r\n\r\n<p style="text-align:justify">&ldquo;Ch&uacute;ng ta kh&ocirc;ng đạt được mục ti&ecirc;u v&igrave; ch&uacute;ng ta nỗ lực qu&aacute; nhiều để t&igrave;m kiếm th&agrave;nh c&ocirc;ng. B&iacute; mật - như Ching-Nin-Chu cho biết - l&agrave; t&igrave;m ra điểm c&acirc;n bằng giữa nỗ lực v&agrave; sự thoải m&aacute;i.&rdquo;</p>\r\n\r\n<p style="text-align:justify"><strong>(Success)</strong></p>\r\n', 'lam-it-duoc-nhieu', 17, 1, '2016-11-22 23:31:37', '2016-11-23 19:21:43'),
(24, 'Kính Vạn Hoa  - Tập 15', 70000, 30, 2, '1479958506-kvh_18tap_15.jpg', '<p><span style="background-color:rgb(249, 249, 249); font-family:opensansregular,arial,helvetica,sans-serif; font-size:12px">K&iacute;nh Vạn Hoa (Phi&ecirc;n Bản 18 Tập) - Tập 15</span></p>\r\n\r\n<p style="text-align:justify">Bộ ấn phẩm K&iacute;nh Vạn Hoa phi&ecirc;n bản mới 18 tập, minh họa của họa sĩ Đỗ Ho&agrave;ng Tường.</p>\r\n\r\n<p style="text-align:justify">&ldquo;Khi viết bộ s&aacute;ch nhiều tập d&agrave;nh cho thiếu nhi&nbsp;<em>K&iacute;nh vạn hoa</em>, việc đầu ti&ecirc;n của Nguyễn Nhật &Aacute;nh l&agrave; mua đủ s&aacute;ch gi&aacute;o khoa từ lớp 1 đến lớp 112 v&agrave; những loại s&aacute;ch li&ecirc;n quan đến từng m&ocirc;n học được giảng dạy trong nh&agrave; trường. Bạn đọc thiếu nhi th&iacute;ch th&uacute; với nh&acirc;n vật Qu&yacute; r&ograve;m trong bộ s&aacute;ch n&agrave;y v&igrave; những tr&ograve; ảo thuật độc đ&aacute;o, đ&oacute; l&agrave; nhờ anh căn cứu v&agrave;o t&agrave;i liệu tham khảo về c&aacute;c phản ứng ho&aacute; học. Để chắc chắn hơn, anh đ&atilde; nhờ một kỹ sư ho&aacute; học xem lại mức độ ch&iacute;nh x&aacute;c của n&oacute;. Hơn thế nữa, c&aacute;c phản ứng ho&aacute; học đ&oacute; phải d&ugrave;ng đ&uacute;ng t&ecirc;n gọi trong nh&agrave; trường, chứ kh&ocirc;ng d&ugrave;ng theo t&ecirc;n gọi của c&aacute;c nh&agrave; nghi&ecirc;n cứu. Điều n&agrave;y cho thấy Nguyễn Nhật &Aacute;nh rất chu đ&aacute;o, kỹ lưỡng với từng chi tiết trong t&aacute;c phẩm của m&igrave;nh. Do đ&oacute;, kh&ocirc;ng phải ngẫu nhi&ecirc;n m&agrave; 4 tập đầu trong bộ truyện d&agrave;i&nbsp;<em>K&iacute;nh vạn hoa</em>&nbsp;đ&atilde; lập một kỷ lục trong l&atilde;nh vực xuất bản hiện n&agrave;y: 100.000 bản !&rdquo; - Nh&agrave; thơ L&ecirc; Minh Quốc</p>\r\n', 'kinh-van-hoa-tap-15', 15, 1, '2016-11-23 20:34:32', '2016-11-23 20:35:06'),
(25, 'Những Cô Em Gái', 32000, 10, 10, '1479958569-image_73747.jpg', '<p><span style="background-color:rgb(249, 249, 249); font-family:opensansregular,arial,helvetica,sans-serif; font-size:12px">Những C&ocirc; Em G&aacute;i l&agrave; phần tiếp theo của truyện Hoa Hồng Xứ Kh&aacute;c. Truyện n&agrave;y nội dung l&atilde;ng mạn v&agrave; c&oacute; nhiều vần thơ hay. Muốn biết cuộc phi&ecirc;u lưu t&igrave;nh cảm mới của anh ch&agrave;ng Khoa ra sao v&agrave; kết quả c&oacute; tốt đẹp hơn mối t&igrave;nh đầu ti&ecirc;n của anh ch&agrave;ng hay kh&ocirc;ng th&igrave; mời c&aacute;c bạn h&atilde;y c&ugrave;ng đ&oacute;n đọc truyện n&agrave;y. Th&uacute;c sẽ cố gắng đăng xong truyện n&agrave;y sớm để c&aacute;c bạn kh&ocirc;ng phải chờ d&agrave;i cổ như những truyện kh&aacute;c</span></p>\r\n', 'nhung-co-em-gai', 15, 1, '2016-11-23 20:36:09', '2016-11-23 20:36:09'),
(26, 'Đi Qua Hoa Cúc', 87000, 20, 2, '1479958644-image_73749.jpg', '<p><span style="background-color:rgb(249, 249, 249); font-family:opensansregular,arial,helvetica,sans-serif; font-size:12px">Cuốn Đi Qua Hoa C&uacute;c l&agrave; tập truyện d&agrave;i của Nguyễn Nhật &Aacute;nh, mở đầu c&acirc;u truyện t&aacute;c giả kể lại tuổi ấu thơ hồn nhi&ecirc;n của nh&acirc;n vật trong truyện, kết hợp với tả cảnh ở miền qu&ecirc;, những ng&ocirc;i nh&agrave; nằm dọc hai b&ecirc;n đường đ&aacute; sỏi dọc theo hai b&ecirc;n h&agrave;ng d&acirc;m bụt v&agrave; cả c&acirc;y sứ c&acirc;y b&agrave;ng tỏa b&oacute;ng m&aacute;t, tỏa hương thơm trước s&acirc;n nh&agrave;. Một n&eacute;t vẽ n&ecirc;n thơ thật đầm ấm ở một v&ugrave;ng qu&ecirc; xa x&ocirc;i t&aacute;c giả dường như l&agrave;m ấm l&ograve;ng cho người đọc. Thật vậy mỗi cốt truyện của Nguyễn Nhật &Aacute;nh đ&atilde; ph&aacute;c họa l&ecirc;n một n&eacute;t qu&ecirc; hương ngọt ng&agrave;o, một thời ấu thơ đẹp, một t&igrave;nh y&ecirc;u của tuổi học tr&ograve; cũng h&ograve;a lẫn t&igrave;nh y&ecirc;u kh&aacute;t khao của bao lứa tuổi. Cuốn truyện d&agrave;i Đi Qua Hoa C&uacute;c l&agrave; một trong những t&aacute;c phẩm tuyệt t&aacute;c hay của t&aacute;c giả l&agrave;m th&ocirc;i th&uacute;c người đọc th&ecirc;m nhiều ấn tượng v&agrave; sự l&ocirc;i cuốn tr&agrave;n d&acirc;ng trong l&ograve;ng bạn đọc Trước nh&agrave; b&agrave; nội t&ocirc;i c&oacute; một c&acirc;y b&agrave;ng cao thật cao. Mỗi lần về thăm nội, khi chiếc xe gobel của ba t&ocirc;i ngoặt quanh c&aacute;i giếng đ&aacute; đầu l&agrave;ng, bao giờ t&ocirc;i cũng nhấp nhổm ở y&ecirc;n sau v&agrave; hồi hộp ngước mắt tr&ocirc;ng l&ecirc;n. So với d&atilde;y h&agrave;ng r&agrave;o d&acirc;m bụt của những ng&ocirc;i nh&agrave; nằm dọc hai b&ecirc;n con đường đ&aacute; sỏi, kể cả ngọn sầu đ&ocirc;ng v&agrave; c&acirc;y sứ trắng toả hương thơm nức mũi trước s&acirc;n nh&agrave; b&agrave; t&ocirc;i l&uacute;c n&agrave;o cũng vươn cao sừng sững. Khi nh&igrave;n l&ecirc;n, hễ thấy t&aacute;n b&agrave;ng xanh um kia hiện ra trong tầm mắt như một chấm đen mỗi l&uacute;c một lớn dần, t&ocirc;i biết ngay đ&atilde; sắp đến nh&agrave; b&agrave;. V&agrave; thế l&agrave; t&ocirc;i kh&ocirc;ng n&eacute;n nổi nụ cười sung sướng. V&agrave; cả e thẹn nữa, chẳng hiểu v&igrave; sao. Những l&uacute;c đ&oacute;, bao giờ t&ocirc;i cũng &uacute;p mặt v&agrave;o lưng ba t&ocirc;i để giấu đi nỗi xao xuyến của m&igrave;nh. Cũng như vậy, trước ng&otilde; nh&agrave; &ocirc;ng ngoại t&ocirc;i l&agrave; một h&agrave;ng r&agrave;o hoa giấy đỏ. Hoa kh&ocirc;ng thẫm, chỉ đỏ hồng. V&igrave; trồng l&acirc;u năm n&ecirc;n c&acirc;y uốn lượn chằng chịt, gốc n&agrave;o gốc nấy to bằng bắp ch&acirc;n người. Hoa rực rỡ từng ch&ugrave;m, từng nh&aacute;nh, phủ k&iacute;n cả hai trụ cổng bằng đ&aacute; ong l&acirc;u ng&agrave;y l&ecirc;n r&ecirc;u xanh mướt. Qu&ecirc; nội t&ocirc;i thuộc một l&agrave;ng miền n&uacute;i. Qu&ecirc; ngoại t&ocirc;i ở miệt đồng bằng. Nh&agrave; &ocirc;ng t&ocirc;i ở c&aacute;ch đường quốc lộ non một c&acirc;y số về ph&iacute;a biển. Nhưng v&igrave; kh&ocirc;ng bị c&acirc;y cối che khuất n&ecirc;n đứng tr&ecirc;n đường người ta vẫn c&oacute; thể tr&ocirc;ng thấy r&otilde; mồn một vừng hoa đỏ ối dưới kia. Sau n&agrave;y, khi đ&atilde; đi xa, mỗi lần về thăm ngoại, t&ocirc;i ngồi tr&ecirc;n xe đ&ograve; băng qua cầu Cẩm Lễ, mắt n&ocirc;n nao ng&oacute;ng về ph&iacute;a biển, hễ thấy hoa đỏ vẫy tay l&agrave; biết đ&atilde; tới nh&agrave;.</span></p>\r\n', 'di-qua-hoa-cuc', 15, 1, '2016-11-23 20:37:24', '2016-11-23 20:37:24'),
(27, 'Bồ Câu Không Đưa Thư', 30000, 10, 10, '1479958767-image_73749.jpg', '<p><span style="background-color:rgb(249, 249, 249); font-family:opensansregular,arial,helvetica,sans-serif; font-size:12px">Hẳn bạn đọc ở lứa tuổi học tr&ograve; đ&atilde; từng quen thuộc với t&aacute;c giả Nguyễn Nhật Anh với những truyện ngắn v&agrave; truyện d&agrave;i đậm chất học tr&ograve; như: Trại hoa v&agrave;ng, Ch&uacute; b&eacute; rắc rối, Ph&ograve;ng trọ ba người, Bồ c&acirc;u kh&ocirc;ng đưa thư, Những ch&agrave;ng trai xấu t&iacute;nh, C&ocirc; g&aacute;i đến từ h&ocirc;m qua, Trước v&ograve;ng chung kết, Hạ đỏ, Hoa hồng xứ kh&aacute;c, Buổi chiều Windows, Ut Quy&ecirc;n v&agrave; t&ocirc;i... Với lối viết dung dị v&agrave; c&aacute;ch chọn đề t&agrave;i gần gũi với lứa tuổi học tr&ograve;, như quan hệ thầy tr&ograve;, trường lớp, bạn b&egrave;, gia đ&igrave;nh... n&ecirc;n những truyện của &ocirc;ng chiếm được cảm t&igrave;nh của rất nhiều bạn đọc nhỏ tuổi. Họ h&agrave;o hứng v&agrave; n&oacute;ng l&oacute;ng đ&oacute;n đọc c&aacute;c trang viết của &ocirc;ng. Ch&uacute;ng t&ocirc;i sẽ lần lượt giới thiệu với bạn đọc c&aacute;c t&aacute;c phẩm của &ocirc;ng.</span></p>\r\n', 'bo-cau-khong-dua-thu', 15, 1, '2016-11-23 20:39:27', '2016-11-23 20:39:27'),
(28, 'Tiếng Gọi Của Hoang Dã', 45000, 50, 2, '1482372448-1_p5.jpg', '<p style="text-align:justify">Tiếng Gọi Của Hoang D&atilde;&nbsp;(nguy&ecirc;n bản tiếng Anh:&nbsp;The call of the wild) l&agrave; cuốn tiểu thuyết của nh&agrave; văn Mỹ Jack London. T&aacute;c phẩm kể về những chuyến phi&ecirc;u lưu mạo hiểm của ch&uacute; ch&oacute; Bấc trung th&agrave;nh. Bấc đang sống trong trang trại của một gia đ&igrave;nh gi&agrave;u c&oacute; th&igrave; bị bắt c&oacute;c, trở th&agrave;nh ch&oacute; k&eacute;o xe cho những người đi t&igrave;m v&agrave;ng ở khu Alaska lạnh gi&aacute;. Ở đ&oacute;, Bấc phải học c&aacute;ch đối diện với cuộc đấu tranh sinh tồn v&agrave; trở th&agrave;nh thủ lĩnh của đ&agrave;n ch&oacute;. Thi&ecirc;n nhi&ecirc;n nguy&ecirc;n thủy đ&atilde; đ&aacute;nh thức bản năng của Bấc.</p>\r\n\r\n<p style="text-align:justify">Sau một lần đi săn trở về, Bấc nh&igrave;n thấy c&aacute;i chết thương t&acirc;m của Tho&oacute;ctơn - người chủ n&oacute; y&ecirc;u thương nhất. T&igrave;nh y&ecirc;u thương, sự trung th&agrave;nh m&agrave; Bấc d&agrave;nh cho&nbsp;Tho&oacute;ctơn trở th&agrave;nh nỗi đau thống thiết, Bấc trở n&ecirc;n hoang d&atilde; hơn bao giờ hết...Kh&ocirc;ng c&ograve;n mối li&ecirc;n hệ n&agrave;o n&iacute;u Bấc lại với con người, n&oacute; bị cuốn theo tiếng gọi của hoang d&atilde;, cuối c&ugrave;ng trở th&agrave;nh một con s&oacute;i hoang.</p>\r\n\r\n<p style="text-align:justify">Xuất bản lần đầu năm 1903,&nbsp;Tiếng Gọi Của Hoang D&atilde;&nbsp;đ&atilde; trở th&agrave;nh kiệt t&aacute;c cuốn h&uacute;t biết bao thế hệ người đọc. Đến với t&aacute;c phẩm, bạn đọc sẽ c&ugrave;ng Bấc phi&ecirc;u lưu tới những miền đất hoang sơ, hiểu thế n&agrave;o l&agrave; lao khổ, t&igrave;nh y&ecirc;u thương, sự trung th&agrave;nh v&agrave; kh&aacute;t vọng tự do.</p>\r\n', '$str', 11, 1, '2016-11-23 20:41:39', '2016-12-21 19:07:28'),
(29, 'Mặt Nạ Tử Thần Đỏ', 60000, 5, 6, '1479958991-image_136.png', '<p style="text-align:justify">Mặt Nạ Tử Thần Đỏ</p>\r\n\r\n<p style="text-align:justify">Tập truyện mở đầu bằng một vụ c&aacute;c cược kỳ quặc, một ng&oacute;n tay đổi lấy một chiếc xe Cadillac sang trọng mới cứng, sau đ&oacute; l&agrave; vụ trốn chạy ly kỳ của người vợ trẻ khỏi g&atilde; s&aacute;t nh&acirc;n đội lốt một người chồng &acirc;n cần v&agrave; chung thủy, rồi đến sự thoắt ẩn thoắt hiện của g&atilde; l&ugrave;n lưng g&ugrave; xấu x&iacute;, kẻ đ&atilde; ph&aacute; hủy ho&agrave;n to&agrave;n cuộc sống của một người đ&agrave;n &ocirc;ng xa lạ, chỉ v&igrave; anh ta l&agrave; người duy nhất nh&igrave;n thấy hắn trong đ&aacute;m đ&ocirc;ng&hellip; Những c&acirc;u chuyện được kể trong Mặt nạ tử thần đỏ khiến độc giả kh&ocirc;ng khỏi c&oacute; cảm gi&aacute;c thế giới n&agrave;y đầy rẫy những c&acirc;u chuyện kỳ lạ v&agrave; kh&oacute; hiểu, ngay đến người trong cuộc đ&ocirc;i khi cũng kh&ocirc;ng thể t&igrave;m thấy lời giải đ&aacute;p.</p>\r\n', 'mat-na-tu-than-do', 11, 1, '2016-11-23 20:43:12', '2016-11-23 20:43:12'),
(30, 'Hang Dã Thú', 152000, 15, 6, '1479959088-hdt.jpg', '<p style="text-align:justify">Hang D&atilde; Th&uacute;</p>\r\n\r\n<p style="text-align:justify">Hang d&atilde; th&uacute; kể về nh&acirc;n vật Paul Schumann, một người Mỹ gốc Đức đang sống tại New York năm 1936. Nhiệm vụ của g&atilde; l&agrave; giữ vững lương t&acirc;m, đạo đức giữa lừa dối v&agrave; phản bội đang ho&agrave;nh hạnh tại Berlin. Điệp vụ đầu ti&ecirc;n của g&atilde; l&agrave; giết chết kẻ s&aacute;t hại cha m&igrave;nh để b&aacute;o th&ugrave; &ndash; nhưng rồi g&atilde; thấy m&igrave;nh c&oacute; t&agrave;i trong việc n&agrave;y. G&atilde; trở th&agrave;nh một s&aacute;t thủ nổi tiếng với những chiến thuật, đảm nhiệm những phi vụ &ldquo;đ&uacute;ng đắn&rdquo;.</p>\r\n\r\n<p style="text-align:justify">V&agrave; rồi g&atilde; bị t&oacute;m. Tay sĩ quan bắt g&atilde; đề nghị một lựa chọn kh&oacute; khăn: ăn cơm t&ugrave; hoặc đảm nhận một nhiệm vụ ngầm của Ch&iacute;nh phủ. Sẽ l&agrave; nhiệm vụ cuối c&ugrave;ng rồi g&atilde; được nghỉ hưu, sẽ về xưởng in c&ugrave;ng em trai g&atilde; v&agrave; bắt đầu một cuộc sống kh&aacute;c. Vấn đề l&agrave; kh&aacute;ch h&agrave;ng của g&atilde; kh&ocirc;ng phải lũ du thu du thực. M&agrave; l&agrave; Ch&iacute;nh phủ Mỹ.</p>\r\n\r\n<p style="text-align:justify">Trải qua 48 giờ truy đuổi c&acirc;n n&atilde;o, g&atilde; biết m&igrave;nh đ&atilde; vướng v&agrave;o một mớ b&ograve;ng bong, một tr&ograve; m&egrave;o vờn chuột, m&agrave; g&atilde; vừa l&agrave; m&egrave;o vừa l&agrave; chuột. G&atilde; chạy khắp Berlin để &ldquo;h&oacute;a kiếp&rdquo; một tay trong Ch&iacute;nh phủ nhưng ch&iacute;nh g&atilde; cũng bị Cảnh s&aacute;t săn l&ugrave;ng khắp nơi...</p>\r\n\r\n<p style="text-align:justify">Vẫn mang một phong c&aacute;ch rất Jeffery Deaver, Hang d&atilde; th&uacute; chắc chắn l&agrave; một cuốn s&aacute;ch khiến bạn kh&ocirc;ng thể rời mắt.</p>\r\n', 'hang-da-thu', 11, 1, '2016-11-23 20:44:48', '2016-11-23 20:44:48'),
(31, 'Thuyết Sao Cho Phục', 65000, 35, 3, '1479959248-kt.jpg', '<p>Thuyết Sao Cho Phục</p>\r\n', 'thuyet-sao-cho-phuc', 17, 1, '2016-11-23 20:47:28', '2016-11-23 20:47:28');
INSERT INTO `products` (`id`, `name`, `price`, `sale`, `number`, `picture`, `content`, `keywords`, `id_sub`, `status`, `created_at`, `updated_at`) VALUES
(32, 'Thám Tử Lừng Danh Conan (Tập 1)', 45000, 10, 10, '1479959572-cn1.jpg', '<p style="text-align:justify">Nhận lời đề nghị ph&aacute; &aacute;n gi&uacute;p một người đ&agrave;n &ocirc;ng b&iacute; ẩn, Conan v&agrave; mọi người đ&atilde; l&ecirc;n đường tới Yokohama. N&agrave;o ngờ ở đ&oacute; c&oacute; một c&aacute;i bẫy đ&atilde; được giăng sẵn, ran v&agrave; đội th&aacute;m tử nh&iacute; bị giữ l&agrave;m con tin... Trong v&ograve;ng 12 tiếng đồng hồ, nếu vụ &aacute;n kh&ocirc;ng được l&agrave;m s&aacute;ng tỏ, t&iacute;nh mạng c&aacute;c con tin sẽ bị đe dọa. Heiji v&agrave; Kaito Kid cũng xuất hiện, những suy luận sống c&ograve;n của c&aacute;c th&aacute;m tử tại Yokohama bắt đầu.</p>\r\n\r\n<p style="text-align:justify">Mời c&aacute;c bạn c&ugrave;ng thưởng thức m&agrave;n ph&aacute; &aacute;n của d&agrave;n sao th&aacute;m tử, trong t&aacute;c phẩm thứ 10 đ&aacute;ng nhớ n&agrave;y.</p>\r\n', 'tham-tu-lung-danh-conan-tap-1-khuc-nhac-cau-sieu', 21, 1, '2016-11-23 20:52:52', '2016-11-26 22:47:24'),
(33, 'Thám Tử Lừng Danh Conan Tập 4', 18000, 10, 10, '1480225489-cn4.jpg', '<p style="text-align:justify"><span style="color:rgb(255, 102, 0); font-size:medium"><strong><a href="http://tiki.vn/conan-tham-tu-lung-danh/c1087" style="box-sizing: border-box; background-color: transparent; color: rgb(51, 122, 183); text-decoration: none;">Th&aacute;m Tử Lừng Danh Conan</a>&nbsp;Tập 4 (T&aacute;i Bản 2014)</strong></span></p>\r\n\r\n<p style="text-align:justify"><strong>Th&aacute;m Tử Lừng Danh Conan</strong>&nbsp;l&agrave; một bộ&nbsp;<a href="http://tiki.vn/truyen-tranh/c1084" style="box-sizing: border-box; background-color: transparent; color: rgb(51, 122, 183); text-decoration: none;">truyện tranh</a>&nbsp;trinh th&aacute;m Nhật Bản của t&aacute;c giả&nbsp;<strong>Aoyama G&otilde;sh&otilde;.</strong></p>\r\n\r\n<p style="text-align:justify">Nh&acirc;n vật ch&iacute;nh của truyện l&agrave; một th&aacute;m tử học sinh trung học c&oacute; t&ecirc;n l&agrave; Kudo Shinichi - th&aacute;m tử học đường xuất sắc - một lần bị bọn tội phạm &eacute;p uống thuốc độc v&agrave; bị teo nhỏ th&agrave;nh học sinh tiểu học lấy t&ecirc;n l&agrave; Conan Edogawa v&agrave;<span style="font-size:12px">&nbsp;lu&ocirc;n cố gắng truy t&igrave;m tung t&iacute;ch tổ chức &Aacute;o Đen nhằm lấy lại h&igrave;nh d&aacute;ng cũ.</span></p>\r\n\r\n<p style="text-align:justify"><strong>Conan - Tập 4</strong></p>\r\n\r\n<p style="text-align:justify">Người nhỏ nhưng tr&iacute; tuệ th&igrave; kh&ocirc;ng nhỏ tẹo n&agrave;o đ&acirc;u nh&eacute;!!</p>\r\n\r\n<p style="text-align:justify">Bằng chứng l&agrave; một loạt những b&iacute; ẩn đều lần lượt được t&ocirc;i kh&aacute;m ph&aacute; ra hết! Nhưng ước g&igrave; t&ocirc;i sớm quay trở lại h&igrave;nh dạng ban đầu để lật tẩy danh t&iacute;nh bọn người mặc &aacute;o đen. Cũng l&agrave; để được gặp lại Ran nữa!!</p>\r\n', 'tham-tu-lung-danh-conan', 21, 1, '2016-11-26 22:44:49', '2016-11-26 22:44:49'),
(34, 'Thám Tử Lừng Danh Conan Tập 5 ', 18000, 10, 2, '1480225566-conan5_2.jpg', '<p style="text-align:justify"><span style="color:rgb(255, 102, 0); font-size:medium"><strong><a href="http://tiki.vn/conan-tham-tu-lung-danh/c1087" style="box-sizing: border-box; background-color: transparent; color: rgb(51, 122, 183); text-decoration: none;">Th&aacute;m Tử Lừng Danh Conan</a>&nbsp;Tập 5 (T&aacute;i Bản 2014)</strong></span></p>\r\n\r\n<p style="text-align:justify"><strong>Th&aacute;m Tử Lừng Danh Conan</strong>&nbsp;l&agrave; một bộ&nbsp;<a href="http://tiki.vn/truyen-tranh/c1084" style="box-sizing: border-box; background-color: transparent; color: rgb(51, 122, 183); text-decoration: none;">truyện tranh</a>&nbsp;trinh th&aacute;m Nhật Bản của t&aacute;c giả&nbsp;<strong>Aoyama G&otilde;sh&otilde;.</strong></p>\r\n\r\n<p style="text-align:justify">Nh&acirc;n vật ch&iacute;nh của truyện l&agrave; một th&aacute;m tử học sinh trung học c&oacute; t&ecirc;n l&agrave; Kudo Shinichi - th&aacute;m tử học đường xuất sắc - một lần bị bọn tội phạm &eacute;p uống thuốc độc v&agrave; bị teo nhỏ th&agrave;nh học sinh tiểu học lấy t&ecirc;n l&agrave; Conan Edogawa v&agrave;<span style="font-size:12px">&nbsp;lu&ocirc;n cố gắng truy t&igrave;m tung t&iacute;ch tổ chức &Aacute;o Đen nhằm lấy lại h&igrave;nh d&aacute;ng cũ.</span></p>\r\n\r\n<p style="text-align:justify"><strong>Conan - Tập 5</strong></p>\r\n\r\n<p style="text-align:justify">Chưa hết sốc về sự thay đổi h&igrave;nh dạng bản th&acirc;n th&igrave; v&ocirc; số nguy hiểm lại r&igrave;nh rập k&eacute;o đến. T&ocirc;i thật sự kh&ocirc;ng muốn Ran v&igrave; m&igrave;nh m&agrave; bị li&ecirc;n lụy! Nhưng nếu n&oacute; xảy ra, với 7 m&oacute;n đồ hỗ trợ ph&aacute; &aacute;n v&agrave; cả c&aacute;i đầu n&agrave;y nữa t&ocirc;i sẽ quyết chiến với bọn ch&uacute;ng!</p>\r\n\r\n<p style="text-align:justify">T&ocirc;i l&agrave; Edogawa Conan - Th&aacute;m tử nh&iacute; lừng danh!!</p>\r\n', 'tham-tu-lung-danh-conan', 21, 1, '2016-11-26 22:46:06', '2016-11-26 22:46:06'),
(35, 'Thám Tử Lừng Danh Conan Tập 17', 18000, 10, 10, '1480225624-conan17_2.jpg', '<p style="text-align:justify"><span style="color:rgb(255, 102, 0); font-size:medium"><strong><a href="http://tiki.vn/conan-tham-tu-lung-danh/c1087" style="box-sizing: border-box; background-color: transparent; color: rgb(51, 122, 183); text-decoration: none;">Th&aacute;m Tử Lừng Danh Conan</a>&nbsp;Tập 17 (T&aacute;i Bản 2014)</strong></span></p>\r\n\r\n<p style="text-align:justify"><strong>Th&aacute;m Tử Lừng Danh Conan</strong>&nbsp;l&agrave; một bộ&nbsp;<a href="http://tiki.vn/truyen-tranh/c1084" style="box-sizing: border-box; background-color: transparent; color: rgb(51, 122, 183); text-decoration: none;">truyện tranh</a>&nbsp;trinh th&aacute;m Nhật Bản của t&aacute;c giả&nbsp;<strong>Aoyama G&otilde;sh&otilde;.</strong></p>\r\n\r\n<p style="text-align:justify">Nh&acirc;n vật ch&iacute;nh của truyện l&agrave; một th&aacute;m tử học sinh trung học c&oacute; t&ecirc;n l&agrave; Kudo Shinichi - th&aacute;m tử học đường xuất sắc - một lần bị bọn tội phạm &eacute;p uống thuốc độc v&agrave; bị teo nhỏ th&agrave;nh học sinh tiểu học lấy t&ecirc;n l&agrave; Conan Edogawa v&agrave;<span style="font-size:12px">&nbsp;lu&ocirc;n cố gắng truy t&igrave;m tung t&iacute;ch tổ chức &Aacute;o Đen nhằm lấy lại h&igrave;nh d&aacute;ng cũ.</span></p>\r\n\r\n<p style="text-align:justify"><strong>Conan - Tập 17</strong></p>\r\n\r\n<p style="text-align:justify">Tại b&atilde;i biển giữa những ng&agrave;y h&egrave; n&oacute;ng nực sắp xảy ra một chuyện n&agrave;o đ&oacute; chăng!?</p>\r\n\r\n<p style="text-align:justify">Qu&aacute; nhiều người v&agrave; những suy nghĩ, toan t&iacute;nh kh&aacute;c nhau...</p>\r\n\r\n<p style="text-align:justify">Giữa &aacute;nh nắng mặt trời như thi&ecirc;u đốt, c&oacute; ai đ&oacute; đang gặp nguy hiểm...</p>\r\n\r\n<p style="text-align:justify">V&agrave; tớ đ&atilde; vạch trần &acirc;m mưu giết người khiến thủ phạm chẳng c&ograve;n cơ hội tẩu tho&aacute;t nữa!</p>\r\n', 'tham-tu-lung-danh-conan', 21, 0, '2016-11-26 22:47:04', '2016-11-28 19:32:06');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `name`, `picture`, `created_at`, `updated_at`) VALUES
(1, 'Hinh 1', '1482373458-banner1.png', '2016-12-21 19:24:18', '2016-12-21 19:24:18'),
(3, 'Hinh 3', '1482373540-banner3.jpg', '2016-12-21 19:25:40', '2016-12-21 19:25:40'),
(4, 'Hinh 4', '1482373549-banner4.jpg', '2016-12-21 19:25:49', '2016-12-21 19:25:49'),
(5, 'Hinh 5', '1482373559-banner5.jpg', '2016-12-21 19:25:59', '2016-12-21 19:25:59'),
(6, 'Hinh 6', '1482373569-banner6.jpg', '2016-12-21 19:26:09', '2016-12-21 19:26:09'),
(7, 'Hinh 8', '1482374357-banner3.jpg', '2016-12-21 19:33:10', '2016-12-21 19:39:17');

-- --------------------------------------------------------

--
-- Table structure for table `sub_cates`
--

CREATE TABLE `sub_cates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_cate` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_cates`
--

INSERT INTO `sub_cates` (`id`, `name`, `keywords`, `id_cate`, `created_at`, `updated_at`) VALUES
(2, 'Foreign Language Learning', 'foreign-language-learning', 1, '2016-11-17 00:49:16', '2016-11-23 19:33:15'),
(9, 'Bút Ký – Hồi Ký', 'but-ky-hoi-ky', 2, '2016-11-17 21:37:30', '2016-11-23 19:33:31'),
(10, 'Sách Kinh doanh', 'sach-kinh-doanh', 3, '2016-11-17 23:22:34', '2016-11-23 19:32:49'),
(11, 'Sách văn học - tiểu thuyết', 'sach-van-hoc-tieu-thuyet', 5, '2016-11-17 23:41:23', '2016-11-23 19:21:26'),
(13, 'Children', 'Children', 1, '2016-11-18 00:27:04', '2016-11-18 00:27:04'),
(14, 'Light Novel', 'light-novel', 2, '2016-11-18 00:29:03', '2016-11-23 19:21:16'),
(15, 'Nguyễn Nhật Ánh', 'nguyen-nhat-anh', 4, '2016-11-18 00:29:31', '2016-11-23 19:21:12'),
(17, 'Sách Kinh Tế', 'sach-kinh-te', 5, '2016-11-22 20:13:41', '2016-11-23 19:21:05'),
(21, 'Truyện tranh Conan 1', 'truyen-tranh-conan ', 2, '2016-11-23 20:49:54', '2016-12-21 10:04:55');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'superadmin@gmail.com', '$2y$10$.LiZa.0Sm1WDs6toLtkLNuyMp.CfZH8hV3eVDkNBqYc1Ua3WwHPjK', 1, 'VuFkVCCC5qAa8UK0gQOG37ZOMxlB5lHWAA56O3Wpn3FHTmQtKEb8gNvMqZXb', '2016-11-28 01:37:04', '2016-11-28 19:24:16'),
(2, 'admin', 'admin@gmail.com', '$2y$10$.LiZa.0Sm1WDs6toLtkLNuyMp.CfZH8hV3eVDkNBqYc1Ua3WwHPjK', 2, '78d8D5fbRX4TcoizQqd0ZnuxMV8amegAdUrIccfF9MTh3Kxi8daBQYB1XJEO', '2016-11-28 19:17:23', '2016-11-29 20:32:40'),
(3, 'user1', 'user1@gmail.com', '$2y$10$A2mrQj9lqzr1ZHosdAJ86uDSYz.9Y/iLPd4UyNxOSwKHDGGn4w5Am', 3, 'FOkq8m7X3vqb3M3uWzeVo17N5Aug0UsZ80PFk3Bd', '2016-11-28 19:17:50', '2016-11-28 19:17:59'),
(11, 'tinhdang', 'tinhdang@gmail.com', '$2y$10$CVos3T6aOJEh6o/ATusR9.IAMF1fOm1TmtgH0OFpvpJb0j1LmBJb2', 1, NULL, '2016-12-21 23:19:43', '2016-12-21 23:19:43'),
(16, 'user2', 'user2@gmail.com', '$2y$10$T8AWr4/Qfj.VYtlSNR54LefviZa9KK416KFz6Inf0wN7ETBkZrY76', 1, NULL, '2016-12-21 23:38:26', '2016-12-21 23:38:26'),
(17, 'user3', 'user3@gmail.com', '$2y$10$HWgIK0mu/xzX6ELbqDsPEevDzfKew1orhMsPPfuNNwV/2E3o7jXbG', 1, NULL, '2016-12-21 23:39:22', '2016-12-21 23:39:22'),
(21, 'tinhdang123', 'tinhdang@gmail.com', '$2y$10$JDmKV7sPsnOUPuMGGkZceOjc0CUbaVcM4wRKZJ3ft3s2YvJTZefqK', 3, NULL, '2016-12-22 00:30:21', '2016-12-22 00:30:21'),
(22, 'tinhdang345', 'tinhdang@gmail.com', '$2y$10$4CAqYQbt/VxXkL7xwSTJm.2x.rwS8.8isbdZjx.mQJ9kExQZZ0IHi', 3, NULL, '2016-12-22 00:36:09', '2016-12-22 00:36:09'),
(23, 'tinhdang111111', 'admin@gmail.com', '$2y$10$YCUx0JLlUG36K2dMJV8lNevqA61Fc7uh2L.p3ZjLUmAvC1aZVv3/6', 3, NULL, '2016-12-22 00:36:35', '2016-12-22 00:36:35'),
(25, 'tinhdang92', 'tinhdang@gmail.com', '$2y$10$2Zzr6Icr/DZYqhvZWL4IW.4STDs4lxDe4mcFADvEtlOn6E97zlAIm', 3, NULL, '2016-12-22 00:57:26', '2016-12-22 00:57:26');

-- --------------------------------------------------------

--
-- Table structure for table `usersapi`
--

CREATE TABLE `usersapi` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usersapi`
--

INSERT INTO `usersapi` (`id`, `username`, `email`, `password`, `level`, `created_at`, `updated_at`) VALUES
(5, 'admin12', 'superadmin12@gmail.com', '$2y$10$6koaQ5/sQNJCU9LapQhYxeWl1J3VYUpbbt9UKWcZgyx4zIqRApIWy', 1, '2016-12-19 23:51:00', '2016-12-20 02:46:45'),
(6, 'admin', 'superadmin@gmail.com', '$2y$10$pXmpFQK3S78ywm7GftOHdunb3OfZcfXGlC8g7h5kme6VUpUZNQqYK', 2, '2016-12-20 02:43:40', '2016-12-20 02:52:43'),
(8, 'admin14', 'superadmin14@gmail.com', '$2y$10$/9bq4eG5wTcX4OopZuR/d.1q0Pi1IhyAuOvNODCNd39n/ztiCgvHq', 3, '2016-12-20 02:45:05', '2016-12-20 03:17:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acounts`
--
ALTER TABLE `acounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jokes`
--
ALTER TABLE `jokes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_id_sub_foreign` (`id_sub`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_cates`
--
ALTER TABLE `sub_cates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_cates_id_cate_foreign` (`id_cate`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- Indexes for table `usersapi`
--
ALTER TABLE `usersapi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usersapi_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acounts`
--
ALTER TABLE `acounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jokes`
--
ALTER TABLE `jokes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sub_cates`
--
ALTER TABLE `sub_cates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `usersapi`
--
ALTER TABLE `usersapi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_id_sub_foreign` FOREIGN KEY (`id_sub`) REFERENCES `sub_cates` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sub_cates`
--
ALTER TABLE `sub_cates`
  ADD CONSTRAINT `sub_cates_id_cate_foreign` FOREIGN KEY (`id_cate`) REFERENCES `category` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
