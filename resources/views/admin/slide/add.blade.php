@extends('admin.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Slide
            <small>Thêm</small>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-7" style="padding-bottom:120px">
    @if(count($errors)>0)
    <div class="alert alert-danger">
    @foreach($errors->all() as $error)
        <ul>
            <li>{!! $error!!}</li>
        </ul>
    @endforeach
    </div>
    @endif
   
        <form action="" enctype="multipart/form-data" method="POST">
      <input type="hidden" name="_token" value="{!! csrf_token()!!}">
            <div class="form-group">
                <label>Slide Name</label>
                <input class="form-control" name="txtNameSl" placeholder="Please Enter Slide Name" />
            </div>
            <div class="form-group">
              <label>Hình ảnh</label>
              <input type="file" name="fImages">
          </div>
           <button type="submit" class="btn btn-success">Thêm</button>
            <button type="reset" class="btn btn-default">Reset</button>
        <form>
    </div>
</div>
@endsection