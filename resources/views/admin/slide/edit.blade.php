@extends('admin.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Slide
            <small>Sửa</small>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-7" style="padding-bottom:120px">
    @if(count($errors)>0)
    <div class="alert alert-danger">
    @foreach($errors->all() as $error)
        <ul>
            <li>{!! $error!!}</li>
        </ul>
    @endforeach
    </div>
    @endif
   
        <form action="{!! URL::route('admin.slide.postEdit',$id)!!}" enctype="multipart/form-data" method="POST">
      <input type="hidden" name="_token" value="{!! csrf_token()!!}">
            <div class="form-group">
                <label>Slide Name</label>
                <input class="form-control" name="txtNameSl" placeholder="Please Enter Slide Name" value="{!! old('txtNameSl'),isset($data)?$data['name']:NULL!!}"/>
            </div>
            <div class="form-group">
                <label>Hình ảnh cũ</label>
                <img class="img_current" src="{!! asset('public/upload/images/'.$data['picture'])!!}">
                <input type="hidden" name="img_current" value="{!!$data['picture']!!}"/>
            </div>
            <div class="form-group">
              <label>Hình ảnh mới</label>
              <input type="file" name="fImages">
          </div>
           <button type="submit" class="btn btn-success">Sửa</button>
            <button type="reset" class="btn btn-default">Reset</button>
        <form>
    </div>
</div>
@endsection