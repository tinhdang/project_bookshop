@extends('admin.master')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Danh mục sách
            <small>Sửa</small>
        </h1>
    </div>
    <!-- /.col-lg-12 -->
    <div class="col-lg-7" style="padding-bottom:120px">
    @if(count($errors)>0)
    <div class="alert alert-danger">
    @foreach($errors->all() as $error)
        <ul>
            <li>{!! $error!!}</li>
        </ul>
    @endforeach
    </div>
    @endif
        <form action="{!! URL::route('admin.cate.postEdit',$id)!!}" method="POST">
        <input type="hidden" name="_token" value="{!! csrf_token()!!}">
            <div class="form-group">
                <label>Tên danh mục</label>
                <input class="form-control" name="txtCateName" placeholder="Please Enter Category Name" value="{!! old('txtCateName',isset($data)?$data['name']:null)!!}" />
            </div>
            <div class="form-group">
                <label> Keywords</label>
                <input class="form-control" name="txtKeywords" placeholder="Please Enter Category Keywords" value="{!! old('Keywords',isset($data)?$data['keywords']:null)!!}" />
            </div>
           <button type="submit" class="btn btn-success">Sửa</button>
            <button type="reset" class="btn btn-default">Reset</button>
        <form>
    </div>
</div>
@endsection