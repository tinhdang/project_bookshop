@extends('admin.master')
@section('content')
<div class="row">
<div class="col-lg-12">
    <h1 class="page-header">Sản phẩm
        <small>Danh sách</small>
    </h1>
</div>
<!-- /.col-lg-12 -->
<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr align="center">
            <th>#</th>
            <th>Tên sách</th>
            <th>Giá</th>
            <th>Giảm giá</th>
            <th>Số lượng</th>
            <th>Keywords</th>
            <th>Danh mục con</th>
            <th>Date</th>
            <th>Tình trạng</th>            
            <th>Delete</th>
            <th>Edit</th>
        </tr>
  </thead>
  <tbody>
       <?php 
       $stt=0; ?>
      @foreach($data as $item)
      <?php $stt=$stt+1; ?>
    <tr class="odd gradeX" align="center">
      <td>{!! $stt!!}</td>
      <td>{!! $item["name"]!!}</td>
      <td>{!!number_format($item["price"],0,",",".")!!}VNĐ</td>
      <td>{!! $item["sale"]!!}</td>
      <td>{!! $item["number"]!!}</td>
      <td>{!! $item["keywords"]!!}</td>
      <td>
        <?php
          $parent=DB::table('sub_cates')->where('id',$item["id_sub"])->first(); ?>
          {!!$parent->name!!} 
      </td>
      <td>
        <?php  
             echo \Carbon\Carbon::createFromTimeStamp(strtotime($item["created_at"]))->diffforHumans();
          ?>
      </td>
      <td>
      <?php  $status=$item["status"]
      ?>
          @if($status==1)
          Còn hàng
          @else
         Hết hàng
          @endif
      </td>
      <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a onclick="return xacnhanxoa('Bạn có chắc chắn muốn xóa không?')" href="{!!URL::route('admin.product.getDelete',$item['id'])!!}"> Delete</a></td>
      <td class="center"><i class="fa fa-pencil fa-fw"></i><a href="{!!URL::route('admin.product.getEdit',$item['id'])!!}">Edit</a></td>
    </tr>
   @endforeach
  </tbody>
</table>
</div>
@endsection