@extends('admin.master')
@section('content')
<style type="text/css">
  .img_current {
    height: 160px;
    width: 100px;
  }
  .icon-del {
    background: red;
    position: relative;
    top:-70px;
    left: 0px;
  }
</style>
<div class="row">
  <div class="col-lg-12">
      <h1 class="page-header">Sách
          <small>Sửa</small>
      </h1>
  </div>
  <!-- /.col-lg-12 -->
  <div class="col-lg-7" style="padding-bottom:120px">
  @if(count($errors)>0)
    <div class="alert alert-danger">
    @foreach($errors->all() as $error)
        <ul>
            <li>{!! $error!!}</li>
        </ul>
    @endforeach
    </div>
    @endif
       <form name="frmEdit" action="{!! URL::route('admin.product.postEdit',$id)!!}" enctype="multipart/form-data" method="POST">
      <input type="hidden" name="_token" value="{!! csrf_token()!!}">
          <div class="form-group">
              <label>Tên sách </label>
              <input class="form-control" name="txtNamebook" placeholder="Please Enter Username" value="{!! old('txtNamebook'),isset($data)?$data['name']:NULL!!}" />
          </div>
          <div class="form-group">
              <label>Danh mục sách</label>
               <?php $id_sub=$data['id_sub'];?>
              <select class="form-control" name="sltSubcate">
                 @foreach($parent as $item)
                  <option <?php if($id_sub==$item['id']) echo "selected=''";?> value="{!! $item['id']!!}">{!! $item["name"]!!}</option>
                  @endforeach
              </select>
              {{$errors->first('sltSubcate')}}
          </div>
          <div class="form-group">
              <label>Giá (VNĐ)</label>
              <input class="form-control" name="txtPrice" placeholder="Please Enter Price" value="{!! old('txtPrice'),isset($data)?$data['price']:NULL!!}" />
              {!!$errors->first('txtPrice')!!}
          </div>
          <div class="form-group">
              <label>Giảm giá (%)</label>
              <input class="form-control" name="txtSale" placeholder="Please Enter Sale" value="{!! old('txtPrice'),isset($data)?$data['sale']:NULL!!}" />
              {!!$errors->first('txtSale')!!}
          </div>
           <div class="form-group">
              <label>Số lượng</label>
              <input class="form-control" name="txtNumber" placeholder="Please Enter Number" value="{!! old('txtNumber'),isset($data)?$data['number']:NULL!!}"/>
          </div>
          <div class="form-group">
              <label>Nội dung giới thiệu</label>
              <textarea class="form-control" rows="3" name="txtContent">{!! old('txtContent'),isset($data)?$data['content']:NULL!!}</textarea>
              <script type="text/javascript">ckeditor("txtContent");</script>
          </div>
          <div class="form-group">
              <label>Hình ảnh cũ</label>
              <img class="img_current" src="{!! asset('public/upload/images/'.$data['picture'])!!}">
             <!--  <a href="javascript:void(0)" type="button" id="del_img_demo"  class="btn-danger btn-circle icon-del"><i class="fa fa-times"></i></a> -->
              <input type="hidden" class="hinh_current" name="img_current" value="{!!$data['picture']!!}"/>
          </div>
          <div class="form-group">
              <label>Hình ảnh mới</label>
              <input type="file" name="fImages">
          </div>
          <div class="form-group">
              <label> Keywords</label>
              <input class="form-control" name="txtKeywords" placeholder="Please Enter Category Keywords"  value="{!! old('txtKeywords'),isset($data)?$data['keywords']:NULL!!}"/>
          </div>
          <div class="form-group">
              <label>Status</label>
              <?php
                $status=$data['status'];
              ?>
              <label class="radio-inline">
                  <input name="rdoStatus" value="1" <?php if($status == 1) echo "checked=''"; ?> type="radio">Còn hàng
              </label>
              <label class="radio-inline">
                  <input name="rdoStatus" value="0" <?php if($status == 0) echo "checked=''"; ?> type="radio">Hết hàng
              </label>
          </div>
          <button type="submit" class="btn btn-success">Sửa sách</button>
          <button type="reset" class="btn btn-default">Reset</button>
      <form>
  </div>
</div>
 <!-- /.row -->
@endsection