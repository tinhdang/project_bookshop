@extends('admin.master')
@section('content')
<div class="row">
  <div class="col-lg-12">
      <h1 class="page-header">Sách
          <small>Thêm</small>
      </h1>
  </div>
  <!-- /.col-lg-12 -->
  <div class="col-lg-7" style="padding-bottom:120px">
  @if(count($errors)>0)
    <div class="alert alert-danger">
    @foreach($errors->all() as $error)
        <ul>
            <li>{!! $error!!}</li>
        </ul>
    @endforeach
    </div>
    @endif
       <form action="{!! route('admin.product.getAdd')!!}" enctype="multipart/form-data" method="POST">
      <input type="hidden" name="_token" value="{!! csrf_token()!!}">
          <div class="form-group">
              <label>Tên sách </label>
              <input class="form-control" name="txtNamebook" placeholder="Please Enter Username" />
          </div>
          <div class="form-group">
              <label>Danh mục sách</label>
              <select class="form-control" name="sltSubcate">
                  <option value="">Please Choose Category</option>
                  @foreach($parent as $item)
                    <option value="{!!$item['id']!!}">{!!$item["name"]!!}</option>
                  @endforeach
                  
              </select>
          </div>
          <div class="form-group">
              <label>Giá (VNĐ)</label>
              <input class="form-control" name="txtPrice" placeholder="Please Enter Price" />
          </div>
          <div class="form-group">
              <label>Giảm giá (%)</label>
              <input class="form-control" name="txtSale" placeholder="Please Enter Sale" />
          </div>
           <div class="form-group">
              <label>Số lượng</label>
              <input class="form-control" name="txtNumber" placeholder="Please Enter Number" />
          </div>
          <div class="form-group">
              <label>Nội dung giới thiệu</label>
              <textarea class="form-control" rows="3" name="txtContent"></textarea>
              <script type="text/javascript">ckediter("txtContent")</script>
          </div>
          <div class="form-group">
              <label>Hình ảnh</label>
              <input type="file" name="fImages">
          </div>
          <div class="form-group">
              <label> Keywords</label>
              <input class="form-control" name="txtKeywords" placeholder="Please Enter Category Keywords" />
          </div>
          <div class="form-group">
              <label>Status</label>

              <label class="radio-inline">
                  <input name="rdoStatus" value="1" checked="" type="radio">Còn hàng
              </label>
              <label class="radio-inline">
                  <input name="rdoStatus" value="0" type="radio">Hết hàng
              </label>
          </div>
          <button type="submit" class="btn btn-success">Thêm sách</button>
          <button type="reset" class="btn btn-default">Reset</button>
  </div>
</div>
<div class="col-md-1"></div>
<div class="col-md-4">
@for($i=1;$i<=5;$i++)
  <div class="form-group">
    <label>Image Product Detail {!! $i!!}</label>
    <input type="file" name="fProductDetail[]">
  </div>
  @endfor
</div>
<form>
 <!-- /.row -->
@endsection