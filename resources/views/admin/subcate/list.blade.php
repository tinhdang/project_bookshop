@extends('admin.master')
@section('content')
<div class="row">
  <div class="col-lg-12">
      <h1 class="page-header">Danh mục con
          <small>List</small>
      </h1>
  </div>
  <!-- /.col-lg-12 -->
  <table class="table table-striped table-bordered table-hover" id="dataTables-example">
      <thead>
          <tr align="center">
              <th>#</th>
              <th>Tên danh mục con</th>
              <th>Danh mục</th>
              <th>Keywords</th>
              <th>Delete</th>
              <th>Edit</th>
          </tr>
      </thead>
      <tbody>
       <?php $stt=0?>
      @foreach($data as $item)
      <?php $stt=$stt+1?>
          <tr class="odd gradeX" align="center">
              <td>{!! $stt !!}</td>
              <td>{!! $item["name"]!!}</td>
              <td>
                <?php
                  $parent=DB::table('category')->where('id',$item["id_cate"])->first();
                  echo $parent->name;
                  ?>
              </td>
              <td>{!! $item["keywords"]!!}</td>
              <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a onclick="return xacnhanxoa('Bạn có chắc chắn muốn xóa không?')" href="{!!URL::route('admin.subcate.getDelete',$item['id'])!!}"> Delete</a></td>
              <td class="center"><i class="fa fa-pencil fa-fw"></i><a href="{!!URL::route('admin.subcate.getEdit',$item['id'])!!}">Edit</a></td>
          </tr>
      @endforeach
      </tbody>
  </table>
  </div>
  @endsection