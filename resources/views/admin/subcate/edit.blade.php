@extends('admin.master')
@section('content')
<div class="row">
<div class="col-lg-12">
    <h1 class="page-header">Danh mục con
        <small>Sửa</small>
    </h1>
</div>
<!-- /.col-lg-12 -->
<div class="col-lg-7" style="padding-bottom:120px">
    @if(count($errors)>0)
    <div class="alert alert-danger">
    @foreach($errors->all() as $error)
        <ul>
            <li>{!! $error!!}</li>
        </ul>
    @endforeach
    </div>
    @endif
    <form action="/bookshop/admin/subcate/edit/{!!$id!!}" method="POST">
    <input type="hidden" name="_token" value="{!! csrf_token()!!}">
      <div class="form-group">
        <label>Danh mục</label>
         <?php $id_cate=$data['id_cate']?>
        <select class="form-control" name="sltCate">
             @foreach($parent as $item)
              <option <?php if($id_cate==$item['id']) echo "selected=''";?> value="{!! $item['id']!!}">{!! $item["name"]!!}</option>
              @endforeach
        </select>
      </div>
      <div class="form-group">
        <label>Tên danh mục con</label>
        <input class="form-control" name="txtSubName" placeholder="Please Enter Category Name" value="{!! old('txtSubName',isset($data)?$data['name']:null)!!}"/>
      </div>
      <div class="form-group">
          <label>Category Keywords</label>
          <input class="form-control" name="txtKeywords" placeholder="Please Enter Category Keywords" value="{!! old('txtKeywords',isset($data)?$data['keywords']:null)!!} "/>
      </div>
        <button type="submit" class="btn btn-success">Sửa</button>
        <button type="reset" class="btn btn-default">Reset</button>
    <form>
</div>
</div>
@endsection