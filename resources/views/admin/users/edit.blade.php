@extends('admin.master')
@section('content')
<div class="row">
<div class="col-lg-12">
<h1 class="page-header">User
    <small>Edit</small>
</h1>
</div>
<!-- /.col-lg-12 -->
<div class="col-lg-7" style="padding-bottom:120px">
@if(count($errors)>0)
    <div class="alert alert-danger">
    @foreach($errors->all() as $error)
        <ul>
            <li>{!! $error!!}</li>
        </ul>
    @endforeach
    </div>
@endif
<form action="" method="POST">
    <input type="hidden" name="_token" value="{!! csrf_token()!!}">
    <div class="form-group">
        <label>Username</label>
        <input class="form-control" name="txtUser" readonly="" value="{!! $data['username']!!}" />
    </div>
    <div class="form-group">
        <label>Password</label>
        <input type="password" class="form-control" name="txtPass" placeholder="Please Enter Password" />
    </div>
    <div class="form-group">
        <label>RePassword</label>
        <input type="password" class="form-control" name="txtRePass" placeholder="Please Enter RePassword" />
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="email" class="form-control" name="txtEmail" placeholder="Please Enter Email" value="{!! $data['email']!!}" />
    </div>
    @if(Auth::user()->id!=$id)
    <div class="form-group">
        <label>User Level</label>
        <label class="radio-inline">
            <input name="rdoLevel" value="1" type="radio" <?php if($data['level']==1){echo 'checked=""';}?> >Superadmin
        </label>
        <label class="radio-inline">
            <input name="rdoLevel" value="2" <?php if($data['level']==2){echo 'checked=""';}?> type="radio">Admin
        </label>
        <label class="radio-inline">
            <input name="rdoLevel" value="3" <?php if($data['level']==3){echo 'checked=""';}?> type="radio">Member
        </label>
    </div>
    @endif
    <button type="submit" class="btn btn-success">User Add</button>
    <button type="reset" class="btn btn-default">Reset</button>
<form>
</div>
</div>
@endsection