@extends('public.pages')
@section('title','Tài khoản - Bookshop Đà Nẵng')
@section('description','Đây là website bán sách trực tuyến')
@section('content')
<div class="row box_product">
  <h3 class="title_main col-md-4 col-sm-5"> 
    <span class="glyphicon glyphicon-book"></span>
    Tài khoản
  </h3>
</div>

<div class="container-fluid">
  <h2 style="color: #9e3b3b;text-align: center;">Sửa tài khoản</h2>
 @if(count($errors)>0)
    <div class="alert alert-danger">
    @foreach($errors->all() as $error)
        <ul>
            <li>{!! $error!!}</li>
        </ul>
    @endforeach
    </div>
    @endif
  <form method="POST" action="<?php echo URL('acount').'/'.$acount_one->id ?>">
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="_token" value="{!! csrf_token()!!}">
    <p class="row">
      <label class="col-md-2">Username</label>
      <input  class="col-md-6" type="text" name="txtusername" readonly="" value="{!! $acount_one->username!!}" placeholder="Vui lòng nhập tên">
     </p>
      <p class="row">
    <label class="col-md-2">Password</label>
    <input class="col-md-6" type="password" name="txtpassword" placeholder="Vui lòng nhập password">
     </p>
      <p class="row">
    <label class="col-md-2">Email</label>
    <input class="col-md-6"  type="text" name="txtemail" value="{!! $acount_one->email!!}" placeholder="Vui lòng nhập email" >
     </p>
    <input type="submit" class="btn btn-success" value="Sửa"></input>
  <div class="table-responsive">
  </form>
  <form method="POST" action="<?php echo URL('acount').'/'.$acount_one->id ?>">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{!! csrf_token()!!}">
    <input type="submit" class="btn btn-danger" value="Xóa"></input>
  </form>
  </div>
</div>
@endsection