@extends('public.pages')
@section('title','Tìm kiếm - Bookshop Đà Nẵng')
@section('description','Đây là website bán sách trực tuyến')
@section('content')
<div class="row box_product">
  <h3 class="title_main col-md-4 col-sm-5"> 
  <span class="glyphicon glyphicon-book"></span>
 Kết quả tìm kiếm</h3>
</div>
<div class="swiper-container container-fluid">
  <div class="swiper-wrapper">
    <div class="swiper-slide container">
    <br>
      <p>Có <b>{!!count($data)!!}</b> kết quả được tìm kiếm với từ khóa <b>{!!$txt!!}</b></p>
      @foreach($data as $item)
      <div class="col-sm-3 col-md-3 content_ebook col-xs-5">
        <a href="{!!URL('chi-tiet',[$item->id,$item->keywords])!!}"><img src="{{asset('public/upload/images/'.$item->picture)}}" class="img_ebook" alt=""></a>
        <p class="title">{!! $item->name!!}</p>
        <div class="price">
        <span class="price-sell">{!!$item->price!!}₫</span>
        <span class="price-sale">-{!!$item->sale!!}%</span>
        </div>
        <strike>{!!$item->price!!}₫</strike>
        <button type="button" class="btn btn-default btn-sm">
        <span class="glyphicon glyphicon-shopping-cart"></span><a href="{!!URL('chi-tiet',[$item->id,$item->keywords])!!}">Shopping</a>  
        </button>
      </div>
    @endforeach
    </div>
  </div>
<!-- Add Pagination -->
</div>
@endsection