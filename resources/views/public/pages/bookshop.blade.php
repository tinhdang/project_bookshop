@extends('public.master')
@section('title','Bookshop Đà Nẵng')
@section('description','Đây là website bán sách trực tuyến')
@section('content')
<?php $menu_pro=DB::table('category')->orderBy('name','DESC')->get();  ?>
@foreach($menu_pro as $menu)
<?php $id_cate=$menu->id; ?>
<div class="main_cate container-fluid">
  <div class="row box_product">
    <h3 class="title_main col-md-4 col-sm-5"> 
    <span class="glyphicon glyphicon-book"></span>
    {!! $menu->name!!}</h3>
    <div class="product-box-sub">
      <ul>
        <?php $menu_lv2=DB::table('sub_cates')->where('id_cate',$id_cate)->orderBy('id','ASC')->skip(0)->take(2)->get(); ?>
        @foreach($menu_lv2 as $item_menu)
        <?php $id_sub=$item_menu->id; ?>
        <li class="active"><a class="title_pro" href="{!! URL('danh-muc',[$item_menu->id,$item_menu->keywords])!!}">{!! $item_menu->name!!}</a></li>
        @endforeach
      </ul>
      <button type="button" class="btn btn-default btn-sm">Xem thêm
        <span class="glyphicon glyphicon-chevron-right"></span> 
      </button>
    </div>
  </div>
  <?php $product=DB::table('products')->where('id_sub',$id_sub)->orderBy('id','DESC')->skip(0)->take(4)->get(); ?>
  @foreach($product as $item_pro)
  <div class="product">
    <div class="col-sm-3 content_ebook col-xs-6">
      <a href="{!!URL('chi-tiet',[$item_pro->id,$item_pro->keywords])!!}"><img src="{{asset('public/upload/images/'.$item_pro->picture)}}" class="img_pro" alt="{!!$item_pro->name!!}"></a>
      <p class="title">{!! $item_pro->name!!}</p>
      <div class="price">
        <span class="price-sell_pro">{!!number_format(($item_pro->price-($item_pro->price*$item_pro->sale/100)),0,",",".")!!}₫</span>  
        <strike>{!!number_format($item_pro->price,0,",",".")!!} ₫</strike>
      </div>
      <button type="button" class="btn btn-default btn-sm btn_shop">
        <span class="glyphicon glyphicon-shopping-cart"></span><a href="{!!URL('chi-tiet',[$item_pro->id,$item_pro->keywords])!!}">Shopping</a>  
      </button>
      <span class="discount-label">-{!! $item_pro->sale!!}%</span>
    </div>
  </div>
  @endforeach
</div>
@endforeach
@endsection