@extends('public.pages')
@section('title','Tài khoản - Bookshop Đà Nẵng')
@section('description','Đây là website bán sách trực tuyến')
@section('content')
<div class="row box_product">
  <h3 class="title_main col-md-4 col-sm-5"> 
    <span class="glyphicon glyphicon-book"></span>
    Tài khoản
  </h3>
</div>
<div class="container-fluid">
  <h2 style="color: #9e3b3b;text-align: center;">Đăng ký tài khoản</h2>
 @if(count($errors)>0)
    <div class="alert alert-danger">
    @foreach($errors->all() as $error)
      <ul>
        <li>{!! $error!!}</li>
      </ul>
    @endforeach
    </div>
    @endif
  <form method="POST" action="{!! URL('acount')!!}">
    <input type="hidden" name="_token" value="{!! csrf_token()!!}">
    <p class="row">
      <label class="col-md-2">Username</label>
      <input  class="col-md-6" type="text" name="txtusername" placeholder="Vui lòng nhập username ">
     </p>
      <p class="row">
    <label class="col-md-2">Password</label>
    <input class="col-md-6" type="password" name="txtpassword" placeholder="Nhập Password">
     </p>
      <p class="row">
    <label class="col-md-2">Email</label>
    <input class="col-md-6" type="text" name="txtemail" placeholder="Vui lòng nhập email"  >
     </p>
    <input type="submit" class="btn btn-success" value="Đăng ký"></input>
  <div class="table-responsive">
  </form>
  </div>
<div class="col-lg-12">
    @if(Session::has('flash_message'))
        <div class="alert alert-{{ Session::get('flash_level')}}">
            {!! Session::get('flash_message')!!}
        </div>
    @endif
</div>
</div>
@endsection