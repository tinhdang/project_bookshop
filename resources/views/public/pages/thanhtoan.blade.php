@extends('public.pages')
@section('title','Trang thanh toán - Bookshop Đà Nẵng')
@section('description','Đây là website bán sách trực tuyến')
@section('content')
<div class="row box_product">
  <h3 class="title_main col-md-4 col-sm-5"> 
    <span class="glyphicon glyphicon-book"></span>
    Thanh toán trực tuyến
  </h3>
</div>

<div class="container-fluid">
  <h2 style="color: #9e3b3b;text-align: center;">Thông tin của bạn</h2>
  <p style="text-align: center;color: red;">Vui lòng nhập đầy đủ thông tin để chúng tôi có thể liên lạc và giao hàng sớm nhất cho bạn. Xin cảm ơn !</p>
 @if(count($errors)>0)
    <div class="alert alert-danger">
    @foreach($errors->all() as $error)
        <ul>
            <li>{!! $error!!}</li>
        </ul>
    @endforeach
    </div>
    @endif
  <form method="POST" action="">
    <input type="hidden" name="_token" value="{!! csrf_token()!!}">
    <p class="row">
      <label class="col-md-2">Họ tên</label>
      <input  class="col-md-6" type="text" name="txtName" placeholder="Vui lòng nhập tên">
     </p>
      <p class="row">
    <label class="col-md-2">Số điện thoại</label>
    <input class="col-md-6" type="number" name="txtPhone" placeholder="Vui lòng nhập số điện thoại">
     </p>
      <p class="row">
    <label class="col-md-2">Địa chỉ</label>
    <input class="col-md-6"  type="text" name="txtAddress" placeholder="Vui lòng nhập địa chỉ">
     </p>
      <p class="row">
    <label class="col-md-2">Email</label>
    <input class="col-md-6"  type="text" name="txtEmail" placeholder="Vui lòng nhập email"  >
     </p>
      <p class="row">
    <label class="col-md-2">Ghi chú</label>
    <textarea cols="60" rows="5" name="txaContent"> </textarea>
    </p>
  <div class="table-responsive">
  <h2>Giỏ hàng ( {!! $count!!} sản phẩm) </h2>
     <table class="table">
        <thead>
          <tr>
            <th width="10%">#</th>
            <th width="40%">Tên sách</th>
            <th width="10%">Số lượng</th>
            <th width="10%">Giá tiền</th>
            <th width="10%">Thành tiền</th>
          </tr>
        </thead>
        <tbody>
          <?php $stt=0; ?>
          @foreach($content as $item)
          <?php $stt=$stt+1;
           ?>
          <tr>
            <td>{!!$stt!!}
            <input type="hidden" name="id_book" value="{!! $item->id_sub!!}">
            </td>
            <td>{!! $item->name!!}<br/>
            <img style="width: 100px;height: 150px" src="{{ asset('/public/upload/images/'.$item['attributes']->img)}}">
            </td>
            <td>
              <input type="text" readonly="" name="txtqty" class="qty" size="1" value="{!!$item->quantity!!}">
            </td>
            <td class="">
            <input type="hidden" class="price" type="text" value="{!! $item->price!!}">
              {!! number_format($item->price,'0',',','.') !!} VNĐ
              </td>
            <td>{!! number_format($item->price*$item->quantity,'0',',','.')!!} VNĐ</td>
          </tr>
          @endforeach
          <tr>
            <td colspan="4" style="font-weight: bold;">Tổng tiền</td>
            <td id="total" style="font-weight: bold; color: red">{!! number_format($total,'0',',','.')!!} VNĐ</td>
          </tr>
        </tbody>
      </table>
       <input type="submit" class="btn btn-success" value="Thanh toán"></input>
  </form>
  </div>
</div>
@endsection