@extends('public.pages')
@section('title','Trang giỏ hàng- Bookshop Đà Nẵng')
@section('description','Đây là website bán sách trực tuyến')
@section('content')
<div class="row box_product">
  <h3 class="title_main col-md-4 col-sm-5"> 
    <span class="glyphicon glyphicon-book"></span>
    Mua hàng trực tuyến
  </h3>
  <!-- <div class="product-box-sub">
    <ul>
      <li class="active"><a class="title_pro" href="">Thiếu nhi</a></li>
      <li><a class="title_pro" href="">Truyện</a></li>
      <li><a class="title_pro" href="">Văn học</a></li>
    </ul>
    <button type="button" class="btn btn-default btn-sm">Xem thêm
      <span class="glyphicon glyphicon-chevron-right"></span> 
    </button>
  </div> -->
</div>
<!-- Cart -->
<div class="container-fluid">
  <h2 style="color: #9e3b3b;">Giỏ hàng của bạn</h2>
  <p></p>
  <div class="table-responsive">
    <form method="POST" name="frmCart" action="">
      <input type="hidden" name="_token" value="{!!csrf_token()!!}">
      <table class="table">
        <thead>
          <tr>
            <th width="10%">#</th>
            <th width="40%">Tên sách</th>
            <th width="10%">Số lượng</th>
            <th width="10%">Giá tiền</th>
            <th width="10%">Thành tiền</th>
            <th width="10%">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php $stt=0; ?>
          @foreach($content as $item)
          <?php $stt=$stt+1;
           ?>
          <tr>
            <td>{!!$stt!!}</td>
            <td>{!! $item->name!!}<br/>
            <img style="width: 100px;height: 150px" src="{{ asset('/public/upload/images/'.$item['attributes']->img)}}">
            </td>
            <td>
              <input type="number" min=1 max="{!!$item['attributes']->number!!}" name="txtqty" class="qty" size="1" value="{!!$item->quantity!!}">
             <br> 
            </td>
            <td class="">
            <input type="hidden" class="price" type="text" value="{!! $item->price!!}">
              {!! number_format($item->price,'0',',','.') !!} VNĐ
              </td>
            <td>{!! number_format(($item->price)*($item->quantity),'0',',','.')!!} VNĐ</td>
            <td>
              <a href="{!! URL('xoa-san-pham',['id'=>$item->id])!!}"><span class="delCart glyphicon glyphicon-remove" style="color: red"></span>
              </a>&nbsp
              <a class="updateCart" id="{!!$item->id!!}">
                <span class="glyphicon glyphicon-refresh "></span>
              </a>
            </td>
          </tr>
          @endforeach
          <tr>
            <td colspan="4">Tổng tiền</td>
            <td id="total">{!! number_format($total,'0',',','.')!!} VNĐ</td>
            <td></td>
          </tr>
        </tbody>
      </table>
      <div class="down_price col-md-12">
        <a class="btn btn-info btn-md col-md-2 col-md-offset-4" href="{!! URL('/')!!}">
          <span class="glyphicon glyphicon-refresh"></span> Tiếp tục mua hàng
        </a>
        <a class="btn btn-info btn-md col-sm-2 col-md-offset-2 btn_spcart" href="{!! URL('thanh-toan')!!}">
          <span class="glyphicon glyphicon-shopping-cart" style="color: black"></span> Thanh toán
        </a>
      </div>
    </form>
  </div>
</div>
<!-- End Cart -->
@endsection