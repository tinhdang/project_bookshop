@extends('public.pages')
@section('title','Trang thanh toán - Bookshop Đà Nẵng')
@section('description','Đây là website bán sách trực tuyến')
@section('content')
<div class="row box_product">
  <h3 class="title_main col-md-4 col-sm-5"> 
    <span class="glyphicon glyphicon-book"></span>
    Tài khoản
  </h3>
</div>

<div class="container-fluid">
  <h2 style="color: #9e3b3b;text-align: center;">Đăng nhập</h2>
 @if(count($errors)>0)
    <div class="alert alert-danger">
    @foreach($errors->all() as $error)
        <ul>
            <li>{!! $error!!}</li>
        </ul>
    @endforeach
    </div>
    @endif
   
  <form method="POST" action="{!! URL('acount')!!}">
    <input type="hidden" name="_token" value="{!! csrf_token()!!}">
    <p class="row">
      <label class="col-md-2">Username</label>
      <input  class="col-md-6" type="text" name="txtusername" placeholder="Vui lòng nhập tên">
     </p>
      <p class="row">
    <label class="col-md-2">Password</label>
    <input class="col-md-6" type="number" name="txtpassword" placeholder="Vui lòng nhập số điện thoại">
     </p>
    <input type="submit" class="btn btn-success" value="Đăng nhập"></input>
  <div class="table-responsive">
  </form>
  </div>

</div>
@endsection