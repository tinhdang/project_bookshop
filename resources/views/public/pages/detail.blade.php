@extends('public.pages')
@section('title',"$pro_detail->name - Bookshop Đà Nẵng")
@section('description','Đây là website bán sách trực tuyến')
@section('content')
<div class="row box_product container-fluid">
  <h3 class="title_main col-md-4 col-sm-5"> 
  <span class="glyphicon glyphicon-book"></span>
  Chi tiết</h3>
</div>
<!-- Detail -->
<div class="container-fluid">
  <div class="left col-md-4 col-sm-5">
    <img class="img_detail" src="{!! asset('public/upload/images/'.$pro_detail->picture)!!}">
  </div>
  <div class="right col-md-8 col-sm-7">
    <h3 class="title_detail">{!!$pro_detail->name!!}</h3>
    <div class="col-md-6">
    <p>Tình trạng :<span class="stt_detail">Còn hàng</span></p>
    <p>Giá bìa :  <span class="price_detail"> {!!$pro_detail->price!!} ₫</span></p>
    <p>Giá tại bookshop :<span class="saleoff_detail">{!!($pro_detail->price)-($pro_detail->price*$pro_detail->sale/100)!!} ₫</span></p>
    <p>Tiết kiệm :  <span style="color: red">{!!$pro_detail->sale*$pro_detail->price/100!!} ₫ (-{!!$pro_detail->sale!!}%)</span></p> 
    <form action="{!! URL('mua-hang',[$pro_detail->id,$pro_detail->keywords])!!}" method="GET">
      <input type="hidden" name="_token" value="{!! csrf_token()!!}">
      <label>Số lượng</label>
      <input type="number" min=1  value="1" max="{!!$pro_detail->number!!}" name="txtNumber" style="width: 4em">
      <a class="btn btn-info btn-cart" >
        <span class="glyphicon glyphicon-shopping-cart">
        </span> 
        <button type="submit" id="btn_submit"> Mua ngay</button>
      </a>
    </form>
    </div>
    <div class="col-md-6">
      <i class="fa fa-hand-o-right"></i>
      <p>Thông tin Giao hàng & Khuyến mãi</p>
      <i class="fa fa-hand-o-right"></i>
      <p>- Nhận hàng tại: </p>  
        <i class="fa fa-hand-o-right"></i>
      <p>- Miễn phí giao hàng toàn quốc cho đơn hàng từ 180.000 đ</p>
      <i class="fa fa-hand-o-right"></i>
      <p>Bạn ngại đặt hàng trên website. Gọi 1900 6656, chúng tôi đặt hàng giúp bạn.</p>
    </div>
  </div>
  <div class="col-md-12">
    <h3 class="main_detail">Nội dung chi tiết sách</h3>
    <p style="font-size: 18px">{!!$pro_detail->content!!}</p>
  </div>
  <div class="col-md-12">
    <h3 class="main_detail">Sách cùng loại</h3>
    <section class="slide">
      <div class="swiper-container container-fluid">
          <div class="swiper-wrapper">
            <div class="swiper-slide container">
              @foreach($pro_diff as $diff_pro)
              <div class="content_ebook col-md-2 col-xs-5">
                <a href="{!!URL('chi-tiet',[$diff_pro->id,$diff_pro->keywords])!!}"><img src="{!! asset('public/upload/images/'.$diff_pro->picture)!!}" class="img_ebook" alt=""></a>
                <p class="title">{!! $diff_pro->name!!}</p>
                <div class="price">
                  <span class="price-sell">{!! $diff_pro->price!!} ₫</span>
                  <span class="price-sale">-{!! $diff_pro->sale!!}%</span>
                </div>
                <strike>400.000₫</strike>
                <button type="button" class="btn btn-default btn-sm">
                  <span class="glyphicon glyphicon-shopping-cart"></span> <a href="{!!URL('mua-hang',[$diff_pro->id,$diff_pro->keywords])!!}">Shopping</a>  
                </button>
              </div>
              @endforeach
            </div>
          </div>
    </section>
  </div>
</div>  
<!-- end detail -->
@endsection