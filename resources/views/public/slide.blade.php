
<section class="slide">
  <div class="swiper-container container-fluid">
      <div class="swiper-wrapper">
          <div class="swiper-slide container">
             <div class="col-sm-12 content_ebook col-xs-5">
               <img src="{{url('public/public/images/banner2.jpg')}}">
            </div>
          </div>
          <div class="swiper-slide container">
             <div class="col-sm-2 content_ebook col-xs-5">
               <img src="{{url('public/public/images/banner4.jpg')}}">
            </div>
          </div>
          <div class="swiper-slide container">
             <div class="col-sm-2 content_ebook col-xs-5">
               <img src="{{url('public/public/images/banner5.jpg')}}">
            </div>
          </div>
      </div>
      <!-- Add Pagination -->
      <!-- <div class="swiper-pagination"></div> -->
      <!-- Add Arrows -->
      <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div>
  </div>
</section>