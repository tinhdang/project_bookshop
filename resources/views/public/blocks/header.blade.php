<div class="wrap">
  <header>
    <div class="topnav container-fluid">
      <img class="img_top image-responsive" src="{{url('public/public/images/header.jpg')}}" />
      <nav class="navbar navbar-inverse container-fluid">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{!!url('/')!!}">BookShop</a>
          </div>
          <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
              <li class=""><a class="homepage" href="{!!url('/')!!}">Trang chủ</a></li>
              <?php 
              $menu_vl1=DB::table('category')->orderBy('name','ASC')->get();
               ?>
              @foreach($menu_vl1 as $item)
              <?php $id_lv1=$item->id; ?>
              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">{!! $item->name!!}</span></a>
                <ul class="dropdown-menu">
                  <?php 
                  $menu_lv2=DB::table('sub_cates')->where('id_cate',$id_lv1)->get();
                  ?>
                  @foreach($menu_lv2 as $item2)
                  <li><a href="{!! URL('danh-muc',[$item2->id,$item2->keywords])!!}">{!! $item2->name!!}</a></li>
                  @endforeach
                </ul>
              </li>
              @endforeach
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="{!!URL('acount')!!}">Tài khoản</a></li>
              <li><a href="" class="login_user"></span>Login</a></li>
              <li><a href="{!!URL::route('giohang')!!}"><span class="glyphicon glyphicon-shopping-cart"></span><span style="color: red"> ({!! $count!!})</span></a></li>
              <li><a href="{!!URL('admin/product/list')!!}">Quản trị</a></li>
            </ul>
            <div class="login_form">
              <form action="{!! URL('login')!!}" name="form_login" method="POST" class="">
              <input type="hidden" name="_token" value="{!! csrf_token()!!}">
                <h4>Login</h4>
                <label>Username</label>
                <input type="text" name="userlogin" >
                <label>Password</label>
                <input type="Password" name="passwordlogin">
                <input type="submit" class="login" name="login" value="Login">
              </form>
            <span class="caret-inner"> </span>
           </div>
          </div>
        </div>
      </nav>
    </div>
  </header>
</div>

