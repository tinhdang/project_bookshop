<section class="slide">
  <div class="swiper-container">
    <div class="swiper-wrapper">
      @foreach($slide as $item)
      <div class="swiper-slide container">
        <div class="col-sm-12 col-xs-5">
          <img class="img_banner" src="{{url('public/upload/images/'.$item->picture)}}">
          <!-- banner1.png -->
        </div>
      </div>
      @endforeach
    </div>
    <!-- Add Pagination -->
    <!-- Add Arrows -->
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  </div>
</section>  