<div class="col-md-12 col-sm-12 col-xs-12">
  <form method="GET" action="{!! URL('tim-kiem')!!}">
<input type="hidden" name="_token" value="{!! csrf_token()!!}">
  <label>Tìm kiếm</label>
  <input type="text" name="txtSearch" placeholder="Search">
  <input type="submit" name="timkiem" value="Search">
</form>
</div>
<h2 class="category">
  <span class="glyphicon glyphicon-align-justify click_cate" id="ico_menu"></span>
  <i class="fa fa-dedent"></i>
  Danh mục sách
</h2>
<div id="myCate"> 
  @foreach($cate as $item)
  <?php $id_cate=$item->id; ?>
    <div class="cate_left">
      <i class="fa fa-book" style="font-size:20px;color: #37125f"></i>
      <a  class="cate_link">{!! $item->name!!}</a>
      <?php $menu_lv2=DB::table('sub_cates')->where('id_cate',$id_cate)->get(); ?>
      @foreach($menu_lv2 as $item2)
      <a href="{!! URL('danh-muc',[$item2->id,$item2->keywords])!!}"><p class="menu_lv2">{!! $item2->name!!}</p></a>
      @endforeach
    </div>  
  @endforeach
</div>