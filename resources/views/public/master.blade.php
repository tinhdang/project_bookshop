<!DOCTYPE html>
<html>
<head>
  <title>@yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="@yield('description')">
  <link rel="stylesheet" type="text/css" href="{{url('public/public/css/mystyle.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/public/css/font-awesome/css/font-awesome.min.css')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('public/public/css/swiper.min.css')}}" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<body>

  @include('public.blocks.header')
  <main class="container-fluid">
    <aside class="leftbar col-md-2 col-sm-2 col-xs-12">
    @include('public.blocks.leftbar')
    </aside>
    <!-- Section -->
    @include('public.blocks.slide')
    <!-- LEFTBAR -->
    <!-- Content -->
    <div class="content col-md-10 col-sm-10 col-xs-12">
      <!-- Nội dung chính -->
    @yield('content')
     <!-- End Nội dung chính -->
    </div>
  </main>
  <div class="clear"></div>
  @include('public.blocks.footer')
  <script src="{{url('public/public/js/js/swiper.min.js')}}"></script>
  <script src="{{url('public/public/js/myjavascript.js')}}"></script>
</body>
</html>