<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sub_cates', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('keywords');
			$table->integer('id_cate')->unsigned();
			$table->foreign('id_cate')->references('id')->on('category')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sub_cates');
	}

}
