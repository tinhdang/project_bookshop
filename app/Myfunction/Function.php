<?php 
function stripUnicode($str) {
    if(!$str) return false;
    $unicode = array(
        'a'=>'á|à|ạ|ả|ã|â|ấ|ầ|ậ|ẩ|ẫ|ă|ắ|ằ|ặ|ẳ|ẵ',
        'A'=>'Á|À|Ạ|Ả|Ã|Â|Ấ|Ầ|Ậ|Ẩ|Ẫ|Ă|Ắ|Ằ|Ặ|Ẳ|Ẵ',
        'e'=>'é|è|ẹ|ẻ|ẽ|ê|ế|ề|ệ|ể|ễ',
        'E'=>'É|È|Ẹ|Ẻ|Ẽ|Ê|Ế|Ề|Ệ|Ể|Ễ',
        'o'=>'ó|ò|ọ|ỏ|õ|ô|ố|ồ|ộ|ổ|ỗ|ơ|ớ|ờ|ợ|ở|ỡ',
        'O'=>'Ó|Ò|Ọ|Ỏ|Õ|Ô|Ố|Ồ|Ộ|Ổ|Ỗ|Ơ|Ớ|Ờ|Ợ|Ở|Ỡ',
        'u'=>'ú|ù|ụ|ủ|ũ|ư|ứ|ừ|ự|ử|ữ',
        'U'=>'Ú|Ù|Ụ|Ủ|Ũ|Ư|Ứ|Ừ|Ự|Ử|Ữ',
        'i'=>'í|ì|ị|ỉ|ĩ',
        'I'=>'Í|Ì|Ị|Ỉ|Ĩ',
        'd'=>'đ',
        'd'=>'Đ',
        'y'=>'ý|ỳ|ỵ|ỷ|ỹ',
        'Y'=>'Ý|Ỳ|Ỵ|Ỷ|Ỹ'
      );
    foreach ($unicode as $khongdau=>$codau) {
      $arr=explode("|",$codau);
      $str=str_replace($arr,$khongdau,$str);
      }
      return $str;
}
function changeTitle($str){
  $str=trim($str);
  if($str=="") return "";
  $str=str_replace('"','','$str');
  $str=str_replace("'",'','$str');
  $str=stripUnicode($str);
  $str=mb_convert_case($str,MB_CASE_LOWER,'utf-8');
  $str=str_replace(' ','-',$str);
  return $str;
}
function cate_parent($data){
  foreach ($data as $key=>$value) {
    $name=$value['name'];
    $id=$value['id'];
    echo "<option value='$id'>$name</option>";    
  }
}


?>