<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');
// Route::get('home', 'HomeController@index');
Route::get('mua-hang/{id}/{tensanpham}',['as'=>'muahang','uses'=>'WelcomeController@muahang']);
Route::get('gio-hang',['as'=>'giohang','uses'=>'WelcomeController@giohang']);
Route::get('danh-muc/{id}/{tenloai}',['as'=>'danhmuc','uses'=>'WelcomeController@danhmuc']);
Route::get('chi-tiet/{id}/{tensanpham}',['as'=>'chitiet','uses'=>'WelcomeController@chitiet']);
Route::get('xoa-san-pham/{id}',['as'=>'xoasanpham','uses'=>'WelcomeController@xoasanpham']);
Route::post('cap-nhat','WelcomeController@capnhat');
Route::get('tim-kiem',['as'=>'timkiem','uses'=>'WelcomeController@timkiem']);
Route::get('thanh-toan',['as'=>'thanhtoan','uses'=>'WelcomeController@thanhtoan']);
Route::post('thanh-toan',['as'=>'thanhtoan','uses'=>'WelcomeController@Postthanhtoan']);
Route::controllers([	
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
Route::group(['prefix'=>'admin'],function(){
	Route::group(['prefix'=>'cate'],function(){
		Route::get('list',['as'=>'admin.cate.list','uses'=>'CateController@getList']);
		Route::get('add',['as'=>'admin.cate.getAdd','uses'=>'CateController@getAdd']);
		Route::post('add',['as'=>'admin.cate.postAdd','uses'=>'CateController@postAdd']);
		Route::get('delete/{id}',['as'=>'admin.cate.getDelete','uses'=>'CateController@getDelete']);
		Route::get('edit/{id}',['as'=>'admin.cate.getEdit','uses'=>'CateController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.cate.postEdit','uses'=>'CateController@postEdit']);
	});
	Route::group(['prefix'=>'subcate'],function(){
		Route::get('list',['as'=>'admin.subcate.list','uses'=>'SubcateController@getList']);
		Route::get('add',['as'=>'admin.subcate.getAdd','uses'=>'SubcateController@getAdd']);
		Route::post('add',['as'=>'admin.subcate.postAdd','uses'=>'SubcateController@postAdd']);
		Route::get('delete/{id}',['as'=>'admin.subcate.getDelete','uses'=>'SubcateController@getDelete']);
		Route::get('edit/{id}',['as'=>'admin.subcate.getEdit','uses'=>'SubcateController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.subcate.postEdit','uses'=>'SubcateController@postEdit']);
	});
	Route::group(['prefix'=>'product'],function(){
		Route::get('list',['as'=>'admin.product.list','uses'=>'ProductController@getList']);
		Route::get('add',['as'=>'admin.product.getAdd','uses'=>'ProductController@getAdd']);
		Route::post('add',['as'=>'admin.product.postAdd','uses'=>'ProductController@postAdd']);
		Route::get('delete/{id}',['as'=>'admin.product.getDelete','uses'=>'ProductController@getDelete']);
		Route::get('edit/{id}',['as'=>'admin.product.getEdit','uses'=>'ProductController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.product.postEdit','uses'=>'ProductController@postEdit']);
	});
	Route::group(['prefix'=>'slide'],function(){
		Route::get('list',['as'=>'admin.slide.list','uses'=>'SlideController@getList']);
		Route::get('add',['as'=>'admin.slide.getAdd','uses'=>'SlideController@getAdd']);
		Route::post('add',['as'=>'admin.slide.postAdd','uses'=>'SlideController@postAdd']);
		Route::get('delete/{id}',['as'=>'admin.slide.getDelete','uses'=>'SlideController@getDelete']);
		Route::get('edit/{id}',['as'=>'admin.slide.getEdit','uses'=>'SlideController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.slide.postEdit','uses'=>'SlideController@postEdit']);
	});
	Route::group(['prefix'=>'user'],function(){
		Route::get('list',['as'=>'admin.user.list','uses'=>'UserController@getList']);
		Route::get('add',['as'=>'admin.user.getAdd','uses'=>'UserController@getAdd']);
		Route::post('add',['as'=>'admin.user.postAdd','uses'=>'UserController@postAdd']);
		Route::get('delete/{id}',['as'=>'admin.user.getDelete','uses'=>'UserController@getDelete']);
		Route::get('edit/{id}',['as'=>'admin.user.getEdit','uses'=>'UserController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.user.postEdit','uses'=>'UserController@postEdit']);
	});
});
Route::group(['middleware'=>'cors','prefix'=>'api/v1'], function()
{
    Route::resource('authenticate','AuthenticateController',['only'=>['index']]);
    Route::post('authenticate','AuthenticateController@authenticate');
    Route::get('authenticate/user','AuthenticateController@getAuthenticatedUser');
});
Route::group(['middleware'=>'cors','prefix'=>'api/v1'],function(){
Route::resource('jokes', 'JokesController');
});
Route::group(['prefix'=>'/'],function(){
    Route::resource('acount','HomepageController');
    Route::resource('login','AcountController');
});
