<?php namespace App\Http\Controllers;
use DB,Mail;
use Cart;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Order;
use App\Http\Requests\OrderRequest;
class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
		$slide=DB::table('slides')->orderBy('id','DESC')->get();
		$cate=DB::table('category')->orderBy('name','DESC')->get();
		$count = Cart::getContent()->count();
		view()->share('slide',$slide);
		view()->share('cate',$cate);
		view()->share('count',$count);
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cate=DB::table('category')->orderBy('name','DESC')->get();
		$product=DB::table('products')->select('id','name','price','sale','number','picture','content','keywords','id_sub','status','created_at','updated_at')->orderBy('id','DESC')->skip(0)->take(4)->get();
		
		$menu_lv2=DB::table('sub_cates')->where('id_cate',$cate['0']->id)->orderBy('id','ASC')->get();
		return view('public.pages.bookshop',compact('cate','product','menu_lv2'));
		
	}
	public function danhmuc($id) 
	{
		$sub_cate=DB::table('sub_cates')->select('id','name')->where('id',$id)->get();
		$product_cate=DB::table('products')->select('id','name','price','sale','number','picture','content','keywords','id_sub','status','created_at','updated_at')->where('id_sub',$id)->orderBy('id','DESC')->get();	
		return view('public.pages.category',compact('product_cate','sub_cate'));
	}
	public function muahang(Request $request,$id)
	{
		$product_buy=DB::table('products')->where('id',$id)->first();

		$qty=$request->txtNumber;
		$product_buy->price_sale=$product_buy->price-($product_buy->price*$product_buy->sale/100);
		Cart::add(array('id'=>$product_buy->id,'name'=>$product_buy->name,'price'=>$product_buy->price_sale,'quantity'=>$qty,'attributes'=>array('img'=>$product_buy->picture,'sale'=>$product_buy->sale,'status'=>$product_buy->status,'number'=>$product_buy->number)));
		$content=Cart::getContent();
		return redirect()->route('giohang');
	}
	public function giohang( Request $request)
	{

		if($request->ajax()){
			$number=$request->number;
			return $number;
		}
		$slide=DB::table('slides')->orderBy('id','DESC')->get();
		$cate=DB::table('category')->orderBy('name','DESC')->get();
		$content=Cart::getContent();
		
		$total=Cart::getTotal();
		
		return view('public.pages.cart',compact('content','total'));
	}
	public function chitiet($id) 
	{
		$pro_detail=DB::table('products')->where('id',$id)->orderBy('id','DESC')->first();
		$id_sub=$pro_detail->id_sub;
		$pro_diff=DB::table('products')->where('id_sub','=',$id_sub)->where('id','!=',$id)->orderBy('id','DESC')->skip(0)->take(6)->get();
		return view('public.pages.detail',compact('pro_detail','pro_diff'));
	}
	public function xoasanpham($id) 
	{
		Cart::remove($id);
		return redirect()->route('giohang');
	}
	public function capnhat(Request $request) 
	{
		if($request->ajax()){
			$qty=$request->qty; //SL
			$rowid=$request->rowid; //id
			$price_pro=$request->price; // ĐG
			
			// Cart::update($rowid,$qty,$price_pro);
			// Cart::update($rowid, array(
			// 	  'quantity' =>$qty, 
			// 	));
			Cart::update($rowid, array(
			  'quantity' => array(
			      'relative' => false,
			      'value' => $qty
			  ),
			));
			$price=$qty*$price_pro; // SĐG
			$total=Cart::getTotal();
			return [$price,$total];
		}
	}
	public function thanhtoan() {
		$content=Cart::getContent();
		$total=Cart::getTotal();
		return view('public.pages.thanhtoan',compact('content','total'));
	}
	public function Postthanhtoan(OrderRequest $request) {	
		$content=Cart::getContent()->toArray();
		$name=$request->txtName;
		$data=['hoten'=>$request->txtName,'phone'=>$request->txtPhone,'address'=>$request->txtAddress,'email'=>$request->txtEmail,'content'=>$content,'ghichu'=>$request->txaContent];
		Mail::send('public.pages.pages',$data,function($message){
			
			$message->from('tinh10dt2@gmail.com','Đơn Hàng Mới');
			$message->to('visao3062@gmail.com','Admin')->subject('Đơn đặt hàng');
		});
		Cart::clear();
		echo "<script>
			alert('Cám ơn bạn đã đặt hàng, chúng tôi sẽ kiểm tra thông tin và giao hàng đến bạn sớm nhất');
			window.location='".url('/')."'
			</script>";
	}	
	public function timkiem(Request $request)
	{	
		$txt=$request->txtSearch;
		$data=DB::table('products')->where('name', 'like', '%'.$txt.'%')->get();
		return view('public.pages.search',compact('data','txt'));
	}
}
