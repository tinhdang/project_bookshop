<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\SubcateRequest;
use App\Sub_Cate;
use App\Category;
use App\Product;
class SubcateController extends Controller {
	public function getList() {
		
		$data=Sub_Cate::select('id','name','keywords','id_cate')->orderBy('id','DESC')->get()->toArray();
		return view('admin.subcate.list',compact('data'));
	}

	public function getAdd(){
		$parent=Category::select('id','name')->get()->toArray();
		return view('admin.subcate.add',compact('parent'));
	}
	public function postAdd(SubcateRequest $request){
		$subcate= new Sub_Cate; 
		$subcate->id_cate=$request->sltCate;
		$subcate->name=$request->txtSubName;
		$subcate->keywords=$request->txtKeywords;
		$subcate->save();
		return redirect()->route('admin.subcate.list')->with(['flash_level'=>'success','flash_message'=>'Success!! Complete Add']);
	}
	public function getEdit($id){
		$data=Sub_Cate::findOrFail($id)->toArray();
		$parent=Category::select('id','name','keywords')->get()->toArray();
		return view('admin.subcate.edit',compact('data','parent','id'));
	}
	public function postEdit(SubcateRequest $request,$id) {
		$subcate=Sub_Cate::find($id);
		$subcate->id_cate=$request->sltCate;
		$subcate->name=$request->txtSubName;
		$subcate->keywords=$request->txtKeywords;
		$subcate->save();
		return redirect()->route('admin.subcate.list')->with(['flash_level'=>'success','flash_message'=>'Success!! Complete Update !']);
	}
	public function getDelete($id) {
		$parent=Product::where('id_sub',$id)->count();
		if($parent==0){
			$subcate=Sub_Cate::find($id);
		$subcate->delete($id);
		return redirect()->route('admin.subcate.list')->with(['flash_level'=>'success','flash_message'=>'Success ! Complete Delete']);
	}else{
		echo "<script type='text/javascript'>
		alert('Sorry ! Can not delete this Category');
		window.location='";
		echo route('admin.subcate.list');
		
		echo "'
		</script>";
		
		}
		
	}

}
