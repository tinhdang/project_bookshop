<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Product;
use App\Sub_cate;
use File;
use Input;
class ProductController extends Controller {
  
  public function getList(){
   
    $data=Product::select('id','name','price','sale','number','picture','content','keywords','id_sub','status','created_at','updated_at')->orderBy('id','DESC')->get()->toArray();
    return view('admin.product.list',compact('data'));
  }
  public function getAdd(){
    $parent=Sub_cate::select('id','name','id_cate')->orderBy('id_cate','ASC')->get()->toArray();
    return view('admin.product.add',compact('parent'));
  }
  public function postAdd(ProductRequest $request){
   
    $product= new Product;
    $product->name=$request->txtNamebook;
    $product->id_sub=$request->sltSubcate;
    $product->price=$request->txtPrice;
    $product->sale=$request->txtSale;
    $product->number=$request->txtNumber;
    $product->content=$request->txtContent;
    $product->keywords=changeTitle($request->txtKeywords);
    $product->status=$request->rdoStatus;
    if(!empty($request->file('fImages'))){
    $img= $request->file('fImages')->getClientOriginalName();
    $img_new=time().'-'.$img; 
    $product->picture= $img_new;
    $request->file('fImages')->move('public/upload/images',$img_new);
    }
     $product->save();
    return redirect()->route('admin.product.list')->with(['flash_level'=>'success','flash_message'=>'Success ! Complete Add']);
  }
  public function getEdit($id){
    $data=Product::find($id)->toArray();
    $parent=Sub_cate::select('id','name','id_cate','keywords')->get()->toArray();
    return view('admin.product.edit',compact('data','id','parent'));
  }
  public function postEdit(ProductRequest $request,$id){

    $product=Product::find($id);
    $product->name=$request->txtNamebook;
    $product->id_sub=$request->sltSubcate;
    $product->price=$request->txtPrice;
    $product->sale=$request->txtSale;
    $product->number=$request->txtNumber;
    $product->content=$request->txtContent;
    $product->keywords=changeTitle($request->txtKeywords);
    $product->status=$request->rdoStatus;
    $img_current='public/upload/images/'.$request->input('img_current');
    if(!empty($request->file('fImages'))){
      $img_name=$request->file('fImages')->getClientOriginalName();
      $img_new=time().'-'.$img_name;
      $product->picture= $img_new;
    
      $request->file('fImages')->move('public/upload/images',$img_new);
      if(File::exists($img_current)){
        File::delete($img_current);
      }
    }else{
    }
    $product->save();
    return redirect()->route('admin.product.list')->with(['flash_level'=>'success','flash_message'=>'Success ! Complete Update Product']);
  } 
  public function getDelete($id)
  {
    $product=Product::find($id);
    File::delete('public/upload/images/'.$product['picture']);
    $product->delete($id);
    return redirect()->route('admin.product.list')->with(['flash_level'=>'success','flash_message'=>'Success ! Complete Delete Product']);
  }
}
