<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\User;
use Hash;
use Auth;
class UserController extends Controller {
    public function getList() {
        $user=User::select('id','username','level')->orderBy('id','DESC')->get();
        return view('admin.users.list',compact('user'));
    }
    public function getAdd() {

        return view('admin.users.add');
    }
    public function postAdd(UserRequest $request) {
        $user=new User();
        $user->username=$request->txtUser;
        $user->password=Hash::make($request->txtPass);
        $user->email=$request->txtEmail;
        $user->level=$request->rdoLevel;;
        $user->remember_token=$request->_token;
        $user->save();
        return redirect()->route('admin.user.list')->with(['flash_level'=>'success','flash_message'=>'Success ! Complete Add User']);
    }
    
    public function getDelete($id) {
        $user_curent=Auth::user()->id;
        $user=User::find($id);
        if(($id==1)||($user_curent!=1 && $user->level==1)) {
            return redirect()->route('admin.user.list')->with(['flash_level'=>'danger','flash_message'=>'Danger ! Do not Delete']);
        }else {
            $user->delete($id);
            return redirect()->route('admin.user.list')->with(['flash_level'=>'success','flash_message'=>'Success ! Complete Delete User']);
        }
    }
    public function getEdit($id) {

        $data=User::findOrFail($id)->toArray();
        if((Auth::user()->id!=1)&&($id==1||($data['level']==2&&(Auth::user()->id!=$id)))){
           return redirect()->route('admin.user.list')->with(['flash_level'=>'danger','flash_message'=>'Danger ! You can edit this user!!']);
        }
        return view('admin.users.edit',compact('data','id'));
    }
    public function postEdit(Request $request ,$id) {
      $user=User::find($id);
        if($request->input('txtPass')){
          $this->validate($request,
            [
              'txtRePass'=>'same:txtPass'
             ],
            [ 
              'txtRePass.same'=>' The Paswork do not Match'
            ]);
          $pass=$request->input('txtPass');
          $user->password=Hash::make($pass);
        }
        $user->email=$request->txtEmail;
        $user->level=$request->rdoLevel;
        $user->remember_token=$request->input('_token');
        $user->save();
        return redirect()->route('admin.user.list')->with(['flash_level'=>'success','flash_message'=>'Success ! Complete Edit User']);
    }
}
