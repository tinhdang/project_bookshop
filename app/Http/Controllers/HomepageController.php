<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\AcountRequest;
use App\Http\Controllers\Controller;
use Response;
use App\Users;
use App\Order;
use App\Product;
use App\Category;
use App\Sub_Cate;
use App\Slide;
use Hash,Input;
use App\User;
use Cart;
class HomepageController extends Controller {

		public function __construct()
		{
			$this->middleware('guest');
			$slide=Slide::orderBy('id','DESC')->get();
			$cate=Category::orderBy('name','DESC')->get();
			$count=Cart::getContent()->count();
			view()->share('slide',$slide);
			view()->share('cate',$cate);
			view()->share('count',$count);
		}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('public.pages.acount');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(AcountRequest $request)
	{
		$acount= new User; 
		$acount->username=$request->txtusername;
		$acount->email=$request->txtemail;
		$acount->password=Hash::make($request->txtpassword);
		$acount->level='3';
		$acount->save();
		$id=$acount->id;
		return  redirect()->route('acount.edit',['id'=>$id])->with(['flash_level'=>'success','flash_message'=>'Success!! Complete Create Acount']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{	
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$acount_one=User::find($id);
		return view('public.pages.acounted',compact('acount_one'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(AcountRequest $request, $id)
	{	
		$acount = User::find($id);
	    $acount->username = $request->txtusername;
	    $acount->password = Hash::make($request->txtpassword);
	    $acount->email = $request->txtemail;
	    $acount->save();
	    return redirect()->route('acount.index')->with(['flash_level'=>'success','flash_message'=>'	Success!! Complete Edit Acount']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request,$id)
	{
		$acount = User::find($id);
		$acount->delete();
	   return redirect()->route('acount.index')->with(['flash_level'=>'success','flash_message'=>'Delete	Success! ']);
	}

}
