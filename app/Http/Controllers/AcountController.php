<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Acount;
use Illuminate\Http\Request;
use Hash;	
use Cart;
class AcountController extends Controller {
	public function __construct()
		{
			// $this->middleware('guest');
			// $slide=Slide::orderBy('id','DESC')->get();
			// $cate=Category::orderBy('name','DESC')->get();
			// $count=Cart::getContent()->count();
			// view()->share('slide',$slide);
			// view()->share('cate',$cate);
			// view()->share('count',$count);
		}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
			$username=$request->userlogin;
			// $password=Hash::make($request->passwordlogin);
			$password=$request->passwordlogin;
			$acount_one=Acount::where('username','=',$username)->where('password','=',$password)->first();
			if(!$acount_one){
				return redirect()->route('acount.index')->with(['flash_level'=>'danger','flash_message'=>'	Username or Password Not True']);
			}else{
				return redirect()->route('acount.index')->with(['flash_level'=>'success','flash_message'=>'ok','acount_one'=>$acount_one]);

			}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
