<?php namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Slide;
use File;
use App\Http\Requests\SlideRequest;

class SlideController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getList(){
		$data=Slide::select('id','name','picture')->orderBy('id','DESC')->get()->toArray();
		return view('admin.slide.list',compact('data'));
	}
	public function getAdd(){
		return view('admin.slide.add');
	}
	public function postAdd(SlideRequest $request){
		$slide= new Slide;
		$slide->name=$request->txtNameSl;
		$img= $request->file('fImages')->getClientOriginalName();
	    $img_new=time().'-'.$img;
	    $slide->picture= $img_new;
	    $request->file('fImages')->move('public/upload/images',$img_new);
		$slide->save();
		return redirect()->route('admin.slide.list')->with(['flash_level'=>'success','flash_message'=>'Success!! Complete Add Slide']);
	}
	public function getEdit($id){
		$data=Slide::find($id)->toArray();
		return view('admin.slide.edit',compact('data','id'));	
	}
	public function postEdit(SlideRequest $request,$id){
		$data=Slide::find($id);
		$data->name=$request->txtNameSl;
		$img_current='public/upload/images/'.$request->input('img_current');
	    if(!empty($request->file('fImages'))){
	      $img_name=$request->file('fImages')->getClientOriginalName();
	      $img_new=time().'-'.$img_name;
	      $data->picture= $img_new;
	      $request->file('fImages')->move('public/upload/images',$img_new);
	      if(File::exists($img_current)){
	        File::delete($img_current);
	      }
	    }else{
	    }
		$data->save();
		return redirect()->route('admin.slide.list')->with(['flash_level'=>'success','flash_message'=>'Success!! Complete Edit Slide']);
		}
	 public function getDelete($id)
	  {
	    $slide=Slide::find($id);
	    File::delete('public/upload/images/'.$slide['picture']);
	    $slide->delete($id);
	    return redirect()->route('admin.slide.list')->with(['flash_level'=>'success','flash_message'=>'Success ! Complete Delete Product']);
	  }

}
