<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\CateRequest;
use App\Category;
use App\Sub_cate;
class CateController extends Controller {
	public function getList(){
		$data=Category::select('id','name','keywords')->orderBy('id','DESC')->get()->toArray();
		return view('admin.cate.list',compact('data'));
	}

	public function getAdd(){
		return view('admin.cate.add');
	}
	public function postAdd(CateRequest $request){
		$cate= new Category;
		$cate->name=$request->txtCateName;
		$cate->keywords=$request->txtKeywords;
		$cate->save();
		return redirect()->route('admin.cate.list')->with(['flash_level'=>'success','flash_message'=>'Success!! Complete Add']);
	}
	public function getEdit($id){
		$data=Category::find($id)->toArray();
		return view('admin.cate.edit',compact('data','id'));	
	}
	public function postEdit(CateRequest $request,$id){
		$cate=Category::find($id);
		$cate->name=$request->txtCateName;
		$cate->keywords=$request->txtKeywords;
		$cate->save();
		return redirect()->route('admin.cate.list')->with(['flash_level'=>'success','flash_message'=>'Success!! Complete Update']);
	}
	public function getDelete($id) {
		$parent=Sub_cate::where('id_cate',$id)->count();
		if($parent==0){
			$cate=Category::find($id);
			$cate->delete($id);
		return redirect()->route('admin.cate.list')->with(['flash_level'=>'success','flash_message'=>'Success ! Complete Delete ']);
		}else {
			echo "<script type='text/javascript'>
				alert('Sorry ! Cannot delete');
				window.location='";
							echo route('admin.cate.list');
				echo "'
			</script>";
		}

	}
	

}
