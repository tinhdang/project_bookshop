<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UserapiRequest;
use App\Http\Controllers\Controller;
use App\Joke;
use Response;
use App\Users;
use App\Product;
use Hash;
use Input;
class JokesController extends Controller {
	  public function __construct(){
        // //$this->middleware('auth.basic', ['only' => 'store']);
        // $this->middleware('auth.basic');
        // $this->middleware('jwt.auth');

        // $this->middleware('jwt.auth', ['except' => ['authenticate', 'index', 'show']]);
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$search_term = $request->input('search');
		if($search_term) {
			$jokes=Users::orderBy('id','DESC')->where('username', 'LIKE', "%$search_term%")->get();
		}
		else {
			$jokes=Users::all();
		}
    return Response::json($this->transformCollection($jokes), 200);
}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserapiRequest $request)
	{
	    $joke = Users::create($request->all());
	    return Response::json([
	       'message' => 'Joke Created Succesfully',
	        'data' => $this->transform($joke)
	    ]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$joke=Users::find($id);
		if(!$joke){
			return Response::json([
				'error'=>[
					'message'=>'Joke does not exist'
					]
				],404);

		}
		return Response::json([
				'data'=>$this->transform($joke)
			],200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UserapiRequest $request, $id)
	{    
	    
	    $joke = Users::find($id);
	    $joke->username = $request->username;
	    $joke->password = Hash::make($request->password);
	    // $user->password=Hash::make($request->txtPass);
	    $joke->level = $request->level;
	    $joke->email = $request->email;
	    $joke->save();
	    return Response::json([
	        'message' => 'Joke Updated Succesfully',
	        'data' => $this->transform($joke)
	    ]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	    Users::destroy($id);
	}

	private function transformCollection($jokes){
	    return array_map([$this, 'transform'], $jokes->toArray());
	}
	  
	private function transform($joke){
	    return [
	       'joke_id' => $joke['id'],
	       'joke_name' => $joke['username'],
	       'joke_email' => $joke['email'],
	       'joke_level' => $joke['level'],
	       'joke_pass' => $joke['password']
	     ];
	}
}
