<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'txtNamebook'=>'required',
			'sltSubcate'=>'required',
			'txtPrice'=>'required|alpha_num',
			'txtSale'=>'required|alpha_num|digits_between:0,2',
			'txtNumber'=>'required|alpha_num',
			'txtContent'=>'required',
			'fImages'=>'required',
			'txtKeywords'=>'required',
			'rdoStatus'=>'required',
		];
	}
	public function messages()
	{
		return[
			'txtNamebook.required'=>'Vui lòng nhập tên sách',
			'sltSubcate.required'=>'Vui lòng chọn danh mục sách',
			'txtPrice.required'=>'Vui lòng nhập giá	',
			'txtPrice.alpha_num'=>'Hãy nhập một số khác đúng định dạng giá',
			'txtSale.required'=>'Vui lòng nhập số giảm giá ',
			'txtSale.alpha_num'=>'Hãy nhập một số khác đúng định dạng sale',
			'txtSale.digits_between'=>'Nhập số lớn hơn 0 và nhỏ hơn 100',
			'txtNumber.required'=>'Vui lòng nhập số lượng sách ',
			'txtNumber.alpha_num'=>'Hãy nhập số lượng sách đúng định dạng ',
			'txtContent.required'=>'Vui lòng nhập nội dung ',
			'fImages.required'=>'Vui lòng chọn hình ảnh',
			'txtKeywords.required'=>'Vui lòng nhập keywords',
			'rdoStatus.required'=>'Vui lòng chọn trạng thái ',
		];
	}

}
