<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class SubcateRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'sltCate'=>'required',
			'txtSubName'=>'required',
			'txtKeywords'=>'required',
		];
	}
	public function messages()
	{
		return[
		'sltCate.required'=>'Vui lòng chọn danh mục',
		'txtSubName.required'=>'Vui lòng nhập tên danh mục con ',
		'txtKeywords.required'=>'Vui lòng nhập keywords',
		];
	}

}
