<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'txtUser'=>'required|unique:users,username',
			'txtPass'=>'required|same:txtPass',
			'txtRePass'=>'required',
			'txtEmail'=>'required|regex:/^[a-z][a-z0-9]*(_[a-z0-9]+)*(\.[a-z0-9]+)*@[a-z0-9]([a-z0-9][a-z0-9]+)*(\.[a-z]{2,4}){1,2}$/',
			'rdoLevel'=>'required',
		];
	}
	public function messages()
	{
		return [
			'txtUser.required'=>'Nhập User',
			'txtUser.unique'=>'Nhập User khác',
			'txtPass.required'=>'Nhập Pass',
			'txtRePass.required'=>'Nhập lại Pass',
			'txtRePass.same'=>'Nhập lại Pass cho khớp',
			'txtEmail.required'=>'Nhập email',
			'txtEmail.regex'=>'Nhập email đúng định dạng',
			'rdoLevel'=>'Chọn Level',
		];
	}

}
