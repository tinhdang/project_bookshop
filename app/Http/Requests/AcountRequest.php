<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class AcountRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'txtusername'=>'required|unique:users,username',
			'txtpassword'=>'required',
			'txtemail'=>'required|regex:/^[a-z][a-z0-9]*(_[a-z0-9]+)*(\.[a-z0-9]+)*@[a-z0-9]([a-z0-9][a-z0-9]+)*(\.[a-z]{2,4}){1,2}$/',
		];
	}
	public function messages()
	{
		return[
		'txtusername.required'=>'Vui lòng nhập tên',
		'txtpassword.required'=>'Vui lòng nhập pass ',
		'txtemail.required'=>'Vui lòng nhập email',
		'txtusername.unique'=>'Nhập User khác',
		'txtemail.regex'=>'Nhập email đúng định dạng',
		];
	}
}
