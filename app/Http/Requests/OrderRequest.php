<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrderRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'txtName'=>'required',
			'txtPhone'=>'required|alpha_num',
			'txtEmail'=>'required',
			'txtAddress'=>'required',
		];
	}
	public function messages()
	{
		return[
			'txtName.required'=>'Vui lòng nhập tên ',
			'txtPhone.required'=>'Vui lòng nhập SDT',
			'txtPhone.alpha_num'=>'Hãy nhập một số khác đúng định dạng',
			'txtEmail.required'=>'Vui lòng nhập email ',
			'txtAddress.required'=>'Vui lòng nhập số địa chỉ',
		];
	}

}
