<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	protected $table ='products';
	protected $fillable=['name','price','sale','number','picture','content','keywords','id_sub','status'];
	public $timestamps=true;

	public function sub_cate() {
		return $this->belongTo('App\Sub_Cate');
	}
}
