<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model {

	protected $table ='order';
	protected $fillable=['name','phone','email','address','content','id_sub','number'];
	public $timestamps=true;

}
