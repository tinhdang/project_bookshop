<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table ='category';
	protected $fillable=['name','keywords'];
	public $timestamps=true;

	public function sub_cate() {
		return $this->hasMany('App\Product');
	}
}
