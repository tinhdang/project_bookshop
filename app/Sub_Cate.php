<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_Cate extends Model {

	protected $table ='sub_cates';
	protected $fillable=['name','keywords','id_cate'];
	public $timestamps=true;

	public function category() {
		return $this->belongTo('App\Category');
	}
	public function product() {
		return $this->hasMany('App\Sub_Cate');
	}
}