<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model {

	protected $table ='orders';
	protected $fillable=['name','phone','email','address','content','id_sub','number'];
	public $timestamps=true;

}
