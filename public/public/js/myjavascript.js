var swiper = new Swiper('.swiper-container', {
      pagination: '.swiper-pagination',
      slidesPerView: 1,
      paginationClickable: true,
      spaceBetween: 30,
      keyboardControl: true,
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
  });
 $("document").ready(function(){
  $(".click_cate").click(function(){
    $("#myCate").slideToggle();
      return false;
    });  
  });

$('document').ready(function(){
    $(".updateCart").click(function(){
      var rowid=$(this).attr('id');
      var qty=$(this).parent().parent().find('.qty').val();
      var _token=$("input[name='_token']").val();
      var price=$(this).parent().parent().find('.price').val();

      $.ajax({
        url: "cap-nhat",
        type: 'POST',
        cache: false,
        data: {"_token":_token,"rowid":rowid, "qty":qty,"price":price},
        success: function (data){
          $("#"+rowid).parent().prev().html(data['0']+" VNĐ");
          $("#total").html(data['1']+" VNĐ");
        }
      }); 
   });
 }); 
/*Login*/
$("document").ready(function(){
  $(".login_user").click(function(){
    $(".login_form").toggle('fast');
      return false;
    });  
});
